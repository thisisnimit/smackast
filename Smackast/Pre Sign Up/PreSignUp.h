//
//  PreSignUp.h
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUp.h"

@interface PreSignUp : UIViewController{
    NSString *imgURL, *strFullName, *strEmail;
    __block NSString *strLoginWith;
    NSMutableDictionary *dictRememberMe;

}
- (IBAction)btnSignUpWithFacebookClicked:(id)sender;
- (IBAction)btnSignUpClicked:(id)sender;
- (IBAction)btnSignInClicked:(id)sender;

@end
