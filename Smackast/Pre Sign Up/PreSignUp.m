//
//  PreSignUp.m
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "PreSignUp.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface PreSignUp ()

@end

@implementation PreSignUp

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dictRememberMe = [[NSMutableDictionary alloc] init];

}

- (IBAction)btnSignUpWithFacebookClicked:(id)sender {
    [self loginButtonClicked];
}

-(void)loginButtonClicked
{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if (error)
         {
             NSLog(@"Process error");
         } else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
         } else
         {
             NSLog(@"Logged in");
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender "}]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                  if (!error)
                  {
                      NSLog(@"fetched user:%@ ", result);
                      
                      imgURL= [[NSString alloc] initWithFormat: @"http://graph.facebook.com/%@/picture?type=large", [result valueForKey:@"id"]];
                      strEmail = result[@"email"];
                      strFullName = result[@"name"];
                      strLoginWith = @"facebook";
                      [self WSLogin];
                      
                  }
              }];
         }
     }];
    
}

-(void)WSLogin{
    
    NSString *strURL;
    
    strURL = [NSString stringWithFormat:@"%@%@?v_email=%@&e_signup_with=%@&v_password=%@&v_device_token=%@&v_device_type=%@",KBaseURL,KLogin,strEmail,strLoginWith,@"",@"12345",KDeviceType];
       
    NSLog(@"%@",strURL);
    
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [Global callAPIwithURL_PostRequest:strURL completion:^(id responseObject, BOOL sucess)
     {
         [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
         NSLog(@"%@ response :::\n%@",KLogin,responseObject);
         if(sucess)
         {
             if([[responseObject valueForKey:@"success"] integerValue] == 1)
             {
                 [[Global sharedCenter] setIsLoggedIn:YES];
                 [[Global sharedCenter]setStrUserID:[responseObject valueForKey:@"user_id"]];
                 //[Global showToast:[responseObject valueForKey:@"msg"]];
                 
                 [self saveCredentials];
                 [self saveLoginStatus];
                 
                 if ([Global sharedCenter].isVideoInProcess) {
                     [[Global sharedCenter] setIsVideoInProcess:NO];
                     [self.navigationController popViewControllerAnimated:YES];
                 }
                 else{
                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                     ChooseVideo *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"ChooseVideo"];
                     [self.navigationController pushViewController:GoToVC animated:YES];
                 }
             }
             else if([[responseObject valueForKey:@"success"] integerValue] == 3)
             {
                 [[Global sharedCenter]setIsComingFromFacebook:YES];
                 
                 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                 SignUp *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"SignUp"];
                 GoToVC.strName = strFullName;
                 GoToVC.strEmail = strEmail;
                 GoToVC.imgURL = imgURL;
                 GoToVC.strSignUpWith = strLoginWith;
                 [self.navigationController pushViewController:GoToVC animated:YES];
             }
             else
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
             }
         }
         else
         {
             [Global showToast:KMSG_server_is_not_responding];
         }
     }];
}

-(void)saveLoginStatus{
    
    NSMutableDictionary *dictLogin = [[NSMutableDictionary alloc]init];
    
    [dictLogin setValue:@"1" forKey:@"LSLogin"];
    [dictLogin setValue:[Global sharedCenter].strUserID forKey:@"LSUserID"];
    
    NSData *dataRemember = [NSKeyedArchiver archivedDataWithRootObject:dictLogin];
    [[NSUserDefaults standardUserDefaults] setObject:dataRemember forKey:@"NSLoginStatusDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(void)saveCredentials{
    /*
     if (_btnRemember.isSelected) {
     [dictRememberMe setValue:@"1" forKey:@"RMStatus"];
     [dictRememberMe setValue:_txtUserName.text forKey:@"RMemail"];
     [dictRememberMe setValue:_txtPassword.text forKey:@"RMpassword"];
     }
     else{
     [dictRememberMe setValue:@"0" forKey:@"RMStatus"];
     [dictRememberMe setValue:_txtUserName.text forKey:@"RMemail"];
     [dictRememberMe setValue:_txtPassword.text forKey:@"RMpassword"];
     }
     */
    
    //
    [dictRememberMe setValue:@"1" forKey:@"RMStatus"];
    [dictRememberMe setValue:strEmail forKey:@"RMemail"];
    [dictRememberMe setValue:@"" forKey:@"RMpassword"];
    //
    
    NSData *dataRemember = [NSKeyedArchiver archivedDataWithRootObject:dictRememberMe];
    [[NSUserDefaults standardUserDefaults] setObject:dataRemember forKey:@"NSUDRememberMeDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (IBAction)btnSignUpClicked:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    SignUp *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"SignUp"];
    GoToVC.strSignUpWith = @"simple";
    [self.navigationController pushViewController:GoToVC animated:YES];
}

- (IBAction)btnSignInClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
