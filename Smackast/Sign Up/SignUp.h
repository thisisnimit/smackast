//
//  SignUp.h
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseVideo.h"
#import "SignIn.h"
#import "JVFloatLabeledTextField.h"

@interface SignUp : UIViewController{
    NSMutableDictionary *dictRememberMe;
}

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtUserName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtPassword;




- (IBAction)btnSignUpClicked:(id)sender;
- (IBAction)btnSignInClicked:(id)sender;


@property (strong, nonatomic) NSString *strEmail;
@property (strong, nonatomic) NSString *strName;
@property (strong, nonatomic) NSString *imgURL;
@property (strong, nonatomic) NSString *strSignUpWith;

@end
