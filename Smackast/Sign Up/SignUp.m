//
//  SignUp.m
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "SignUp.h"

@interface SignUp ()

@end

@implementation SignUp

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"%@",_strSignUpWith);
    if ([_strSignUpWith isEqualToString:@"facebook"]) {
        _txtEmail.text = _strEmail;
    }
    
    [self setUpPlaceholders];
}

-(void)setUpPlaceholders{
    _txtEmail.floatingLabelActiveTextColor = KAppThemeColor;
    _txtEmail.font = [UIFont fontWithName:KRobotoRegular size:18.0];
    
    _txtPassword.floatingLabelActiveTextColor = KAppThemeColor;
    _txtPassword.font = [UIFont fontWithName:KRobotoRegular size:18.0];
    
    _txtUserName.floatingLabelActiveTextColor = KAppThemeColor;
    _txtUserName.font = [UIFont fontWithName:KRobotoRegular size:18.0];
}

- (IBAction)btnSignUpClicked:(id)sender {
    
    if ([self isValidated]) {
        [self WSSignUP];
    }
}

-(void)WSSignUP{
    
    NSLog(@"%@",_strSignUpWith);
    NSString *strURL = [NSString stringWithFormat:@"%@%@?v_full_name=%@&v_email=%@&v_username=%@&v_password=%@&e_signup_with=%@&v_device_token=%@&v_device_type=%@",KBaseURL,KSignUp,@"NAME",_txtEmail.text, _txtUserName.text, _txtPassword.text, _strSignUpWith, @"12345", KDeviceType];
    
    NSLog(@"%@",strURL);
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [Global callAPIwithURL_PostRequest:strURL completion:^(id responseObject, BOOL sucess)
     {
         [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
         NSLog(@"%@ response :::\n%@",KSignUp,responseObject);
         if(sucess)
         {
             if([[responseObject valueForKey:@"success"] integerValue] == 1)
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
                 
                 [[Global sharedCenter] setIsLoggedIn:YES];
                 [[Global sharedCenter]setStrUserID:[responseObject valueForKey:@"user_id"]];
                 
                 [self saveLoginStatus];
                 
                 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                 ChooseVideo *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"ChooseVideo"];
                 [self.navigationController pushViewController:GoToVC animated:YES];
             }
             else
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
             }
         }
         else
         {
             [Global showToast:KMSG_server_is_not_responding];
         }
     }];
}

-(void)saveLoginStatus{
    
    NSMutableDictionary *dictLogin = [[NSMutableDictionary alloc]init];
    
    [dictLogin setValue:@"1" forKey:@"LSLogin"];
    [dictLogin setValue:[Global sharedCenter].strUserID forKey:@"LSUserID"];
    
    NSData *dataRemember = [NSKeyedArchiver archivedDataWithRootObject:dictLogin];
    [[NSUserDefaults standardUserDefaults] setObject:dataRemember forKey:@"NSLoginStatusDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(void)saveCredentials{
  
    [dictRememberMe setValue:@"1" forKey:@"RMStatus"];
    [dictRememberMe setValue:_txtUserName.text forKey:@"RMemail"];
    [dictRememberMe setValue:_txtPassword.text forKey:@"RMpassword"];
    
    NSData *dataRemember = [NSKeyedArchiver archivedDataWithRootObject:dictRememberMe];
    [[NSUserDefaults standardUserDefaults] setObject:dataRemember forKey:@"NSUDRememberMeDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (IBAction)btnSignInClicked:(id)sender {
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[SignIn class]]) {
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)isValidated{
    
    BOOL result = [[Global sharedCenter] validateEmailWithString:_txtEmail.text];

    if (_txtEmail.text.length == 0) {
        [Global showToast:@"Please enter full username/Email"];
        return NO;
    }
    else if (_txtUserName.text.length == 0){
        [Global showToast:@"Please enter full username"];
        return NO;
    }
    else if(_txtPassword.text.length == 0){
        [Global showToast:@"Please enter password"];
        return NO;
    }
    else if(!result){
        [Global showToast:@"Please enter valid email"];
        return NO;
    }
    
    return YES;
}

@end
