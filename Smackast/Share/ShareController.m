//
//  ShareController.m
//  Smackast
//
//  Created by Nimit on 29/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "ShareController.h"

@interface ShareController ()

@end

@implementation ShareController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _lblVideoName.text = _strVideoName;
    _txtLink.text = _strLink;
}

- (IBAction)btnRecordAnotherVideoClicked:(id)sender {
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[ChooseVideo class]]) {
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}

- (IBAction)btnShareVideoClicked:(id)sender {
    
    NSString *textToShare = @"Hey! Check out this awesome video from Smackast";
    NSURL *myWebsite = [NSURL URLWithString:_txtLink.text];
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)btnCopyLinkClicked:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _txtLink.text;
    [Global showToast:@"Copied!!!"];
}

@end
