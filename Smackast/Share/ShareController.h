//
//  ShareController.h
//  Smackast
//
//  Created by Nimit on 29/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextView.h"
#import "ChooseVideo.h"


@interface ShareController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblVideoName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *txtLink;
@property (weak, nonatomic) IBOutlet UIButton *btnCopyLink;
- (IBAction)btnRecordAnotherVideoClicked:(id)sender;
- (IBAction)btnShareVideoClicked:(id)sender;
- (IBAction)btnCopyLinkClicked:(id)sender;


@property (strong, nonatomic) NSString *strVideoName;
@property (strong, nonatomic) NSString *strLink;

@end
