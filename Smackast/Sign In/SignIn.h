//
//  SignIn.h
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUp.h"
#import "ForgotPassword.h"
#import "ChooseVideo.h"
#import "PreSignUp.h"
#import "JVFloatLabeledTextField.h"

@interface SignIn : UIViewController{
    __block NSString *strLoginWith;
    NSString *imgURL, *strFullName, *strEmail;
    NSMutableDictionary *dictRememberMe;
}

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtPassword;

- (IBAction)btnSignInClicked:(id)sender;
- (IBAction)btnSignInWithFacebookClicked:(id)sender;
- (IBAction)btnSignUpClicked:(id)sender;

@end
