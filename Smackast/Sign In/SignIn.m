//
//  SignIn.m
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "SignIn.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface SignIn ()

@end

@implementation SignIn

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dictRememberMe = [[NSMutableDictionary alloc] init];
    
    //_txtEmail.text = @"nimit";
    //_txtPassword.text = @"123456";
    
    [[Global sharedCenter] setIsComingFromFacebook:NO];
    
    strLoginWith = @"simple";

    [self setCredentials];
    [self retrieveLoginStatus];
    
    [self setUpPlaceholders];
    
}

-(void)setUpPlaceholders{
    _txtEmail.floatingLabelActiveTextColor = KAppThemeColor;
    _txtEmail.font = [UIFont fontWithName:KRobotoRegular size:18.0];
    
    _txtPassword.floatingLabelActiveTextColor = KAppThemeColor;
    _txtPassword.font = [UIFont fontWithName:KRobotoRegular size:18.0];
}

-(void)retrieveLoginStatus{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"NSLoginStatusDetails"];
    NSMutableDictionary *savedRMDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if ([[savedRMDict valueForKey:@"LSLogin"]isEqualToString:@"1"]) {
        [[Global sharedCenter]setIsLoggedIn:YES];
        [[Global sharedCenter] setStrUserID:[savedRMDict valueForKey:@"LSUserID"]];
         
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ChooseVideo *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"ChooseVideo"];
        [self.navigationController pushViewController:GoToVC animated:YES];
    }
    else{
        [[Global sharedCenter]setIsLoggedIn:NO];
    }
}


-(void)setCredentials{
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"NSUDRememberMeDetails"];
    NSMutableDictionary *savedRMDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if ([[savedRMDict valueForKey:@"RMStatus"]isEqualToString:@"1"]) {
        _txtEmail.text = [savedRMDict valueForKey:@"RMemail"];
        _txtPassword.text = [savedRMDict valueForKey:@"RMpassword"];
    }
}

- (IBAction)btnSignInClicked:(id)sender {
    strLoginWith = @"simple";
    
    if ([self isValidated]) {
        [self WSLogin];
    }
}

- (IBAction)btnSignInWithFacebookClicked:(id)sender {
    
    [self loginButtonClicked];
}

-(void)loginButtonClicked
{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if (error)
         {
             NSLog(@"Process error");
         } else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
         } else
         {
             NSLog(@"Logged in");
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender "}]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                  if (!error)
                  {
                      NSLog(@"fetched user:%@ ", result);
                      
                      imgURL= [[NSString alloc] initWithFormat: @"http://graph.facebook.com/%@/picture?type=large", [result valueForKey:@"id"]];
                      strEmail = result[@"email"];
                      strFullName = result[@"name"];
                      strLoginWith = @"facebook";
                      _txtEmail.text = result[@"email"];
                      [self WSLogin];
                      
                  }
              }];
         }
     }];
    
}


-(void)WSLogin{
    
    NSString *strURL;
    
    if ([strLoginWith isEqualToString:@"facebook"]) {
        strURL = [NSString stringWithFormat:@"%@%@?v_email=%@&e_signup_with=%@&v_password=%@&v_device_token=%@&v_device_type=%@",KBaseURL,KLogin,_txtEmail.text,strLoginWith,@"",@"12345",KDeviceType];
    }
    else{
        strURL = [NSString stringWithFormat:@"%@%@?v_email=%@&e_signup_with=%@&v_password=%@&v_device_token=%@&v_device_type=%@",KBaseURL,KLogin,_txtEmail.text,strLoginWith,_txtPassword.text,@"12345",KDeviceType];
    }
    
    NSLog(@"%@",strURL);
    
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [Global callAPIwithURL_PostRequest:strURL completion:^(id responseObject, BOOL sucess)
     {
         [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
         NSLog(@"%@ response :::\n%@",KLogin,responseObject);
         if(sucess)
         {
             if([[responseObject valueForKey:@"success"] integerValue] == 1)
             {
                 [[Global sharedCenter] setIsLoggedIn:YES];
                 [[Global sharedCenter]setStrUserID:[responseObject valueForKey:@"user_id"]];
                 //[Global showToast:[responseObject valueForKey:@"msg"]];
                 
                 [self saveCredentials];
                 [self saveLoginStatus];
                 
                 if ([Global sharedCenter].isVideoInProcess) {
                     [[Global sharedCenter] setIsVideoInProcess:NO];
                     [self.navigationController popViewControllerAnimated:YES];
                 }
                 else{
                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                     ChooseVideo *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"ChooseVideo"];
                     [self.navigationController pushViewController:GoToVC animated:YES];
                 }
             }
             else if([[responseObject valueForKey:@"success"] integerValue] == 3)
             {
                 [[Global sharedCenter]setIsComingFromFacebook:YES];
                 
                 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                 SignUp *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"SignUp"];
                 GoToVC.strName = strFullName;
                 GoToVC.strEmail = strEmail;
                 GoToVC.imgURL = imgURL;
                 GoToVC.strSignUpWith = strLoginWith;
                 [self.navigationController pushViewController:GoToVC animated:YES];
             }
             else
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
             }
         }
         else
         {
             [Global showToast:KMSG_server_is_not_responding];
         }
     }];
}

-(void)saveLoginStatus{
    
    NSMutableDictionary *dictLogin = [[NSMutableDictionary alloc]init];
    
    [dictLogin setValue:@"1" forKey:@"LSLogin"];
    [dictLogin setValue:[Global sharedCenter].strUserID forKey:@"LSUserID"];
    
    NSData *dataRemember = [NSKeyedArchiver archivedDataWithRootObject:dictLogin];
    [[NSUserDefaults standardUserDefaults] setObject:dataRemember forKey:@"NSLoginStatusDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(void)saveCredentials{
    /*
    if (_btnRemember.isSelected) {
        [dictRememberMe setValue:@"1" forKey:@"RMStatus"];
        [dictRememberMe setValue:_txtUserName.text forKey:@"RMemail"];
        [dictRememberMe setValue:_txtPassword.text forKey:@"RMpassword"];
    }
    else{
        [dictRememberMe setValue:@"0" forKey:@"RMStatus"];
        [dictRememberMe setValue:_txtUserName.text forKey:@"RMemail"];
        [dictRememberMe setValue:_txtPassword.text forKey:@"RMpassword"];
    }
    */
    
    //
    [dictRememberMe setValue:@"1" forKey:@"RMStatus"];
    [dictRememberMe setValue:_txtEmail.text forKey:@"RMemail"];
    [dictRememberMe setValue:_txtPassword.text forKey:@"RMpassword"];
    //
    
    NSData *dataRemember = [NSKeyedArchiver archivedDataWithRootObject:dictRememberMe];
    [[NSUserDefaults standardUserDefaults] setObject:dataRemember forKey:@"NSUDRememberMeDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (IBAction)btnSignUpClicked:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    PreSignUp *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"PreSignUp"];
    [self.navigationController pushViewController:GoToVC animated:YES];
}

- (IBAction)btnForgotPasswordClicked:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ForgotPassword *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"ForgotPassword"];
    [self.navigationController pushViewController:GoToVC animated:YES];
}


-(BOOL)isValidated{
    
    if (_txtEmail.text.length == 0) {
        [Global showToast:@"Please enter full username/Email"];
        return NO;
    }
    else if (_txtPassword.text.length == 0){
        if ([strLoginWith isEqualToString:@"facebook"]) {
            return YES;
        }
        else{
            [Global showToast:@"Please enter password"];
            return NO;
        }
    }
    
    return YES;
}

@end
