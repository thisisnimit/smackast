//
//  ResetPassword.h
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseVideo.h"
#import "SignIn.h"
#import "JVFloatLabeledTextField.h"

@interface ResetPassword : UIViewController

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtAuthenticationCode;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtConfirmPassword;



- (IBAction)btnSubmitClicked:(id)sender;


@property (strong, nonatomic) NSString *strEmail;

@end
