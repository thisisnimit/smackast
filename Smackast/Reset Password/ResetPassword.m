//
//  ResetPassword.m
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "ResetPassword.h"

@interface ResetPassword ()

@end

@implementation ResetPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpPlaceholders];
    
}

-(void)setUpPlaceholders{
    _txtAuthenticationCode.floatingLabelActiveTextColor = KAppThemeColor;
    _txtAuthenticationCode.font = [UIFont fontWithName:KRobotoRegular size:18.0];
    
    _txtNewPassword.floatingLabelActiveTextColor = KAppThemeColor;
    _txtNewPassword.font = [UIFont fontWithName:KRobotoRegular size:18.0];
    
    _txtConfirmPassword.floatingLabelActiveTextColor = KAppThemeColor;
    _txtConfirmPassword.font = [UIFont fontWithName:KRobotoRegular size:18.0];
}


- (IBAction)btnSubmitClicked:(id)sender {
    if ([self isValidated]) {
        [self WSResetPassword];
    }
}

-(void)WSResetPassword{
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@?v_authentication_code=%@&v_email=%@&v_new_password=%@",KBaseURL,KResetPassword,_txtAuthenticationCode.text,_strEmail,_txtNewPassword.text];
    NSLog(@"%@",strURL);
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [Global callAPIwithURL_PostRequest:strURL completion:^(id responseObject, BOOL sucess)
     {
         [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
         NSLog(@"%@ response :::\n%@",KResetPassword,responseObject);
         if(sucess)
         {
             if([[responseObject valueForKey:@"success"] integerValue] == 1)
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
                 
                 for (UIViewController *vc in self.navigationController.viewControllers) {
                     if ([vc isKindOfClass:[SignIn class]]) {
                         [self.navigationController popToViewController:vc animated:NO];
                     }
                 }
             }
             else
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
             }
         }
         else
         {
             [Global showToast:KMSG_server_is_not_responding];
         }
     }];
}


-(BOOL)isValidated{
    
    if (_txtAuthenticationCode.text.length == 0) {
        [Global showToast:@"Please enter authentication code"];
        return NO;
    }
    else if(_txtNewPassword.text.length == 0){
        [Global showToast:@"Please enter password"];
        return NO;
    }
    else if(_txtConfirmPassword.text.length == 0){
        [Global showToast:@"Please confirm the password"];
        return NO;
    }
    else if(![_txtNewPassword.text isEqualToString:_txtNewPassword.text]){
        [Global showToast:@"Passwords do not match"];
        return NO;
    }
   
    return YES;
}

@end
