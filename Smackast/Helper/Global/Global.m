//
//  Global.m
//  Smackast
//
//  Created by Raxit on 09/11/16.
//  Copyright © 2016 Raxit. All rights reserved.
//

#import "Global.h"

@implementation Global
static Global *sharedInstance = nil;

+ (Global *)sharedCenter
{
    if (sharedInstance == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedInstance = [[super allocWithZone:NULL] init];
        });
    }
    return sharedInstance;
}


+ (void)callAPIwithURL_PostRequest:(NSString *)strURL completion:(void(^)(id responseObject, BOOL sucess))callback
{    
    if ([[Global sharedCenter] checkInternetConnectivity])
    {
        
    strURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    [request setTimeoutInterval:60];
    
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[strURL length]];
    [request addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: [strURL dataUsingEncoding:NSUTF8StringEncoding]];
    
    strURL = nil;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    AFHTTPResponseSerializer *serializer = [[AFHTTPResponseSerializer alloc] init];
    serializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", @"text/json", @"text/javascript",nil];
    manager.responseSerializer = serializer;
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            
            callback((id)[error localizedDescription],NO);
            
        } else {
            NSData *datat = [NSData dataWithData:responseObject];
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:datat
                                                                 options:kNilOptions
                                                                   error:&error];
            datat = nil;
            callback(json,TRUE);
        }
    }];
    [dataTask resume];
    }
}

+(NSAttributedString*)setTextFieldPlaceHolder:(NSString*)string{
    
    UIColor *color = KGrayPlaceholderColor;
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string
                                                                           attributes:@{
                                                                                        NSForegroundColorAttributeName: color,
                                                                                        NSFontAttributeName : [UIFont fontWithName:KRobotoLight size:15.0]
                                                                                        }
                                            ];
    return attributedString;
    
}

+ (void)showToast:(NSString *)strMessage{
    
    NSDictionary *options = @{
                              kCRToastTextKey : strMessage,
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastFontKey : [UIFont fontWithName:KRobotoMedium size:16.0],
                              kCRToastBackgroundColorKey : [UIColor colorWithHexString:@"0084FF"],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeSpring),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionBottom),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop)
                              };
    [CRToastManager dismissAllNotifications:YES];
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{

                                }];
}

#pragma mark -
#pragma mark EmailValidation

//BOOL result = [APP_DEL validateEmailWithString:@"mayur.patel@sttl.com"];
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9._-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark - Check InternetContectivity


- (BOOL)checkInternetConnectivity
{
    if(KNetIsNotReachable)
    {
        [Global showToast:KMSG_INTERNET_ISSUE];
        return FALSE;
    }
    else
    {
        return TRUE;
    }
    return FALSE;
}



@end
