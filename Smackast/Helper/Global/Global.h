//
//  Global.h
//  Smackast
//
//  Created by Raxit on 09/11/16.
//  Copyright © 2016 Raxit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "define.h"

@interface Global : NSObject
+(Global*)sharedCenter;
+ (void)callAPIwithURL_PostRequest:(NSString *)strURL completion:(void(^)(id responseObject, BOOL sucess))callback;
+(NSAttributedString*)setTextFieldPlaceHolder:(NSString*)string;
- (BOOL)validateEmailWithString:(NSString*)email;
+ (void)showToast:(NSString *)strMessage;
-(BOOL)checkInternetConnectivity;

@property (nonatomic, assign) BOOL isLoggedIn;
@property (strong, nonatomic) NSString *strDeviceID;
@property (strong, nonatomic) NSString *strUserID;

@property (strong, nonatomic) NSString *strCroppedVideoPath;
@property (strong, nonatomic) NSString *strCroppedAudioPath;

@property (nonatomic, assign) BOOL isComingFromFacebook;
@property (nonatomic, assign) BOOL isVideoInProcess;
@property (nonatomic, assign) BOOL isVideoLandscape;


@end
