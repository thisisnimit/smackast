//
//  HeaderViewController.m
//  AdYoYo
//
//  Created by MAYUR PATEL on 9/22/16.
//  Copyright © 2016 SeashoreTech. All rights reserved.
//

#import "CommonHeaderFooterViewController.h"

@interface CommonHeaderFooterViewController ()
@end

@implementation CommonHeaderFooterViewController

#pragma mark -
#pragma mark Class Methods

+(NSString *)className
{
    return  NSStringFromClass([self class]);
}

+(NSString*)HeaderViewIdentifier
{
    return @"HeaderViewController";
}

+(NSString*)FooterViewIdentifier
{
    return @"FooterViewController";
}

#pragma mark -
#pragma mark View Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpHeaderView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setUpHeaderView
{
    if (_headerView==nil)
    {
        _headerView = [[[NSBundle mainBundle] loadNibNamed:[CommonHeaderFooterViewController HeaderViewIdentifier] owner:self options:nil] objectAtIndex:0];
        [_headerView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];        
        [self.view addSubview:_headerView];
    }
}

#pragma mark -
#pragma mark Menu Button

-(void)leftBarButtonIsMenu:(BOOL)isMenu isBack:(BOOL)isBack
{
    if(isMenu)
    {
        [_btnLeftBarButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    }
    else if (isBack)
    {
        [_btnLeftBarButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    }
}



#pragma mark -
#pragma mark Button Action Method

- (IBAction)btnLeftBarButtonTapped:(id)sender
{
    //NSLog(@"%s %d %s %s", __FILE__, __LINE__, __PRETTY_FUNCTION__, __FUNCTION__);
    [self.navigationController popViewControllerAnimated:YES];
}


@end
