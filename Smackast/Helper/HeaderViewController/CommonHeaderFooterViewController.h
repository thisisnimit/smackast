//
//  HeaderViewController.h
//  AdYoYo
//
//  Created by MAYUR PATEL on 9/22/16.
//  Copyright © 2016 SeashoreTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonHeaderFooterViewController : UIViewController

//HeaderView
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *btnLeftBarButton;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleName;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowing;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowers;



- (void)leftBarButtonIsMenu:(BOOL)isMenu isBack:(BOOL)isBack;

@end
