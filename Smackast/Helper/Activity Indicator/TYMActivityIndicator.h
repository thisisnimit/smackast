//
//  TYMActivityIndicator.h
//  BJP
//
//  Created by Mayur Chaniyara on 15/09/15.
//  Copyright (c) 2015 STTL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TYMActivityIndicatorView.h"
#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import "DGActivityIndicatorView.h"

@interface TYMActivityIndicator : NSObject

+(TYMActivityIndicator *) getInstance;

-(void)showActivityIndicator:(UIView*)view;
-(void)hideActivityIndicator:(UIView*)view;

@property (nonatomic) TYMActivityIndicatorView *normalActivityIndicatorView;
@property (nonatomic) DGActivityIndicatorView *activityIndicatorView;

@end
