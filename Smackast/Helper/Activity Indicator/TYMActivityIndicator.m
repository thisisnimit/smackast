//
//  TYMActivityIndicator.m
//  BJP
//
//  Created by Mayur Chaniyara on 15/09/15.
//  Copyright (c) 2015 STTL. All rights reserved.
//

#import "TYMActivityIndicator.h"


#define KActibityIndicator_Height 60

// Center along axis
#define CENTER_VIEW_H(PARENT, VIEW) [PARENT addConstraint:[NSLayoutConstraint constraintWithItem:VIEW attribute: NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:PARENT attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]]
#define CENTER_VIEW_V(PARENT, VIEW) [PARENT addConstraint:[NSLayoutConstraint constraintWithItem:VIEW attribute: NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:PARENT attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]]
#define CENTER_VIEW(PARENT, VIEW) {CENTER_VIEW_H(PARENT, VIEW); CENTER_VIEW_V(PARENT, VIEW);}

// Set Size
#define CONSTRAIN_WIDTH(VIEW, WIDTH) [VIEW addConstraint:[NSLayoutConstraint constraintWithItem:VIEW attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:WIDTH]];
#define CONSTRAIN_HEIGHT(VIEW, HEIGHT) [VIEW addConstraint:[NSLayoutConstraint constraintWithItem:VIEW attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:HEIGHT]];
#define CONSTRAIN_SIZE(VIEW, HEIGHT, WIDTH) {CONSTRAIN_WIDTH(VIEW, WIDTH); CONSTRAIN_HEIGHT(VIEW, HEIGHT);}

@implementation TYMActivityIndicator

@synthesize normalActivityIndicatorView = _normalActivityIndicatorView;

static TYMActivityIndicator *TYMActivityIndicatorVar = nil;

+(TYMActivityIndicator *) getInstance
{
    if(TYMActivityIndicatorVar == nil)
    {
        TYMActivityIndicatorVar = [[super allocWithZone:NULL]init];
    }
    return TYMActivityIndicatorVar;
}

-(id)init
{
    if(self=[super init])
    {
        //Initialize all your global variables here.
        [self normalActivityIndicatorView];
        
        _activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeTriplePulse tintColor:[UIColor colorWithHexString:@"0084FF"] size:50.0f];
        _activityIndicatorView.frame = CGRectMake(0.0f, 0.0f, 300.0f, 300.0f);
    }
    return self;
}

- (TYMActivityIndicatorView *)normalActivityIndicatorView
{
    if (!_normalActivityIndicatorView)
    {
        _normalActivityIndicatorView = [[TYMActivityIndicatorView alloc] initWithActivityIndicatorStyle:TYMActivityIndicatorViewStyleNormal];
        _normalActivityIndicatorView.fullRotationDuration = 1.0;
        //[_normalActivityIndicatorView setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.2]];
        [_normalActivityIndicatorView.layer setCornerRadius:KActibityIndicator_Height/2];
        [_normalActivityIndicatorView setClipsToBounds:YES];
        [_normalActivityIndicatorView setHidesWhenStopped:YES];
        [_normalActivityIndicatorView setClockwise:YES];
        _normalActivityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _normalActivityIndicatorView;
}

-(void)showActivityIndicator:(UIView*)view
{
    
    //[SVProgressHUD show];
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    _activityIndicatorView.center = view.center;
    [view addSubview:_activityIndicatorView];
    [_activityIndicatorView startAnimating];
    
    /*
    [view addSubview:_normalActivityIndicatorView];
    CENTER_VIEW(view, _normalActivityIndicatorView);
    CONSTRAIN_SIZE(_normalActivityIndicatorView, KActibityIndicator_Height, KActibityIndicator_Height);
    [_normalActivityIndicatorView startAnimating];
     */
}

-(void)hideActivityIndicator:(UIView*)view
{
    //[SVProgressHUD dismiss];
    
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [_activityIndicatorView stopAnimating];

    /*
    [_normalActivityIndicatorView stopAnimating];
    [_normalActivityIndicatorView removeFromSuperview];
     */
}

@end
