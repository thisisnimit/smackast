//
//  define.h
//  Smackast
//
//  Created by Raxit on 08/11/16.
//  Copyright © 2016 Raxit. All rights reserved.
//

#ifndef define_h
#define define_h

#import "AppDelegate.h"

#import "NSString+Color.h"
#import "UIColor+HexString.h"

#import "AFNetworking.h"
#import "TYMActivityIndicator.h"
#import "Reachability.h"
#import "CRToast.h"
#import "CRToastLayoutHelpers.h"

#endif /* define_h */
