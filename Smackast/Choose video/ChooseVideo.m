//
//  ChooseVideo.m
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "ChooseVideo.h"
@import MediaPlayer;

static NSString *const AppGroupId = @"group.smackast.share";

@interface ChooseVideo ()<CMMediaPickerControllerDelegate>

//@property (weak, nonatomic) IBOutlet UIImageView *imgViewVideo;
//@property (nonatomic, weak) IBOutlet UIView *videoView;

@property (nonatomic, strong) CMMediaPickerController *mediaPickerController;
@property (nonatomic, assign) CMMediaPickerControllerMediaType selectedMediaType;
@property (nonatomic, assign) CMMediaPickerControllerSourceType selectedSourceType;

//experiment


@end

@implementation ChooseVideo

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self getDirectoryContents];
    [self setUpPlaceholders];
    
    _lblVideoName.text = @"Upload a Video";
    
    [[_btnBrowse layer] setBorderWidth:2.0f];
    [[_btnBrowse layer] setBorderColor:[UIColor colorWithHexString:@"E8EBED"].CGColor];

}

-(void)setUpPlaceholders{
    _txtYouTubeLink.floatingLabelActiveTextColor = KAppThemeColor;
    _txtYouTubeLink.font = [UIFont fontWithName:KRobotoRegular size:18.0];
}

-(void)viewWillAppear:(BOOL)animated{
    [sharedUserDefaults removeObjectForKey:@"SharedExtension"];
    arrSites = [[NSMutableArray alloc]init];
    sharedUserDefaults = [[NSUserDefaults alloc] initWithSuiteName:AppGroupId];
    arrSites = [NSMutableArray arrayWithArray:[sharedUserDefaults valueForKey:@"SharedExtension"]];
    if (arrSites.count>0) {
        _txtYouTubeLink.text = [NSString stringWithFormat:@"%@", [arrSites lastObject]];
    }
    _strVideoUrl = @"";
    _lblVideoName.text = @"Upload a Video";


}
- (IBAction)btnProfileClicked:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ProfileController *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"ProfileController"];
    [self.navigationController pushViewController:GoToVC animated:YES];
}

- (IBAction)btnBrowseClicked:(id)sender {
    videoGalleryViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"videoGalleryViewController"];
    VC.delegate = self;
    [self presentViewController:VC animated:YES completion:nil];
}

-(void)DetermineTheOrientationOfSourceVideo:(NSURL *)videoURL{
    
    AVURLAsset *asset = [AVURLAsset assetWithURL:videoURL];
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    NSLog(@"H : %f W : %f",size.height,size.width);
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if (size.width == txf.tx && size.height == txf.ty){
        NSLog(@"Landscape");
        [[Global sharedCenter] setIsVideoLandscape:true];
    }
    else if (txf.tx == 0 && txf.ty == 0){
        if (size.height>size.width) {
            NSLog(@"Portrait");
            [[Global sharedCenter] setIsVideoLandscape:false];
        }
        else{
            NSLog(@"Landscape");
            [[Global sharedCenter] setIsVideoLandscape:true];
        }
    }
    else if (txf.tx == 0 && txf.ty == size.width){
        NSLog(@"Portrait");
        [[Global sharedCenter] setIsVideoLandscape:false];
    }
    else{
        NSLog(@"Portrait");
        [[Global sharedCenter] setIsVideoLandscape:false];
    }

    NSLog(@"Video orientation is all set! :-)");
}

- (void)selectedVideoURL:(AssetBrowseItem *)item
{
    NSLog(@"Video URL :-> %@", item.videoURL);
    NSLog(@"Video Name :-> %@", item.videoTitle);

    ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
    [assetLibrary assetForURL:item.videoURL resultBlock:^(ALAsset *asset) {
        ALAssetRepresentation *rep = [asset defaultRepresentation];
        Byte *buffer = (Byte*)malloc(rep.size);
        NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
        NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
        
        NSString *storePath = [[self applicationCacheDirectory] stringByAppendingPathComponent:item.videoTitle];
        NSLog(@"New URL :-> %@", storePath);
        [data writeToFile:storePath atomically:YES];
        [self DetermineTheOrientationOfSourceVideo:item.videoURL];

        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            _strVideoUrl = [NSString stringWithFormat:@"%@",storePath];
            _strVideoName = item.videoTitle;
            _strVideoDuration = item.videoLength;
            durationforSlider = item.videoDuration;
            //NSLog(@"Final Video URL :-> %@", _strVideoUrl);
            _lblVideoName.text = [NSString stringWithFormat:@"%@",item.videoTitle];

        });
        
    } failureBlock :^(NSError *err) {
        NSLog(@"Error: %@",[err localizedDescription]);
    }];
}

- (NSString *)applicationCacheDirectory
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return documentsDirectory;
}

-(void)WSGetVideoURL{
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@?url=%@",KBaseURL,KYouTube,_txtYouTubeLink.text];
    NSLog(@"%@",strURL);
    
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [Global callAPIwithURL_PostRequest:strURL completion:^(id responseObject, BOOL sucess)
     {
         NSLog(@"%@ response :::\n%@",KLogin,responseObject);
         if(sucess)
         {
             if([[responseObject valueForKey:@"success"] integerValue] == 1)
             {
                 [self downloadVideoTemp:[responseObject valueForKey:@"youtube_video_url"]];
             }
             
             else
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
                 [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
             }
         }
         else
         {
             [Global showToast:KMSG_server_is_not_responding];
             [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
             
         }
     }];
}

-(void)downloadVideoTemp:(NSString *)fileLink{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:fileLink];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //Update the progress view
            //NSLog(@"%f",downloadProgress.fractionCompleted);
            [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
            [SVProgressHUD showProgress:downloadProgress.fractionCompleted status:[NSString stringWithFormat:@"%.1f%%",downloadProgress.fractionCompleted*100] maskType:SVProgressHUDMaskTypeBlack];
            NSLog(@"%f",downloadProgress.fractionCompleted);
        });
        
    } destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        // Do operation after download is complete
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"Error : %@",error.localizedDescription);
            NSLog(@"response : %@",response);
            
            NSLog(@"File Path : %@",filePath);
            
            if (error == nil) {
                
                _strVideoUrl = filePath.path;
                
                NSURL *sourceMovieURL = [NSURL fileURLWithPath:_strVideoUrl];
                AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:sourceMovieURL options:nil];
                
                [self DetermineTheOrientationOfSourceVideo:sourceMovieURL];

                CMTime duration = sourceAsset.duration;
                float seconds = CMTimeGetSeconds(duration);
                
                durationforSlider = seconds;
                
                NSLog(@"duration: %.2f", seconds);
                
                int totalSeconds = (int)ceilf(seconds);
                NSString *strTotalSeconds = [self timeFormatted:totalSeconds];
                
                _strVideoDuration = strTotalSeconds;
                _strVideoName = @"Youtube Video";
                
                [self performSelectorOnMainThread:@selector(moveToHomeScreen) withObject:nil waitUntilDone:2.0];
                /*
                
                [self performWithAsset:filePath];
                 
                 */
            }
            else{
                NSLog(@"Error : %@",error.localizedDescription);
                //                [Global showToast:[NSString stringWithFormat:@"%@",error.localizedDescription]];
                [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
                [Global showToast:@"Ooops! This video is not available for download!"];
                }
            
        });
        
    }];
    [downloadTask resume];
    
}

//Nimit


- (void)performWithAsset : (NSURL *)moviename
{
    
    self.mutableComposition=nil;
    self.mutableVideoComposition=nil;
    self.mutableAudioMix=nil;
    
    AVAsset *asset = [[AVURLAsset alloc] initWithURL:moviename options:nil];
    
    AVMutableVideoCompositionInstruction *instruction = nil;
    AVMutableVideoCompositionLayerInstruction *layerInstruction = nil;
    CGAffineTransform t1;
    CGAffineTransform t2;
    
    AVAssetTrack *assetVideoTrack = nil;
    AVAssetTrack *assetAudioTrack = nil;
    // Check if the asset contains video and audio tracks
    if ([[asset tracksWithMediaType:AVMediaTypeVideo] count] != 0) {
        assetVideoTrack = [asset tracksWithMediaType:AVMediaTypeVideo][0];
    }
    if ([[asset tracksWithMediaType:AVMediaTypeAudio] count] != 0) {
        assetAudioTrack = [asset tracksWithMediaType:AVMediaTypeAudio][0];
    }
    
    CMTime insertionPoint = kCMTimeZero;
    NSError *error = nil;
    
    
    // Step 1
    // Create a composition with the given asset and insert audio and video tracks into it from the asset
    if (!self.mutableComposition) {
        
        // Check whether a composition has already been created, i.e, some other tool has already been applied
        // Create a new composition
        self.mutableComposition = [AVMutableComposition composition];
        
        // Insert the video and audio tracks from AVAsset
        if (assetVideoTrack != nil) {
            AVMutableCompositionTrack *compositionVideoTrack = [self.mutableComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
            [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [asset duration]) ofTrack:assetVideoTrack atTime:insertionPoint error:&error];
        }
        if (assetAudioTrack != nil) {
            AVMutableCompositionTrack *compositionAudioTrack = [self.mutableComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [asset duration]) ofTrack:assetAudioTrack atTime:insertionPoint error:&error];
        }
        
    }
    
    
    // Step 2
    // Translate the composition to compensate the movement caused by rotation (since rotation would cause it to move out of frame)
    t1 = CGAffineTransformMakeTranslation(assetVideoTrack.naturalSize.height, 0.0);
    float width=assetVideoTrack.naturalSize.width;
    float height=assetVideoTrack.naturalSize.height;
    float toDiagonal=sqrt(width*width+height*height);
    //float toDiagonalAngle = radiansToDegrees(acosf(width/toDiagonal));
    float toDiagonalAngle = RADIANS_TO_DEGREES(acosf(width/toDiagonal));
    float toDiagonalAngle2=90-RADIANS_TO_DEGREES(acosf(width/toDiagonal));
    
    float toDiagonalAngleComple;
    float toDiagonalAngleComple2;
    float finalHeight = 0.0;
    float finalWidth = 0.0;
    
    float degrees=0;
    
    if(degrees>=0&&degrees<=90){
        
        toDiagonalAngleComple=toDiagonalAngle+degrees;
        toDiagonalAngleComple2=toDiagonalAngle2+degrees;
        
        finalHeight=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple)));
        finalWidth=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple2)));
        
        t1 = CGAffineTransformMakeTranslation(height*sinf(DEGREES_TO_RADIANS(degrees)), 0.0);
    }
    else if(degrees>90&&degrees<=180){
        
        
        float degrees2 = degrees-90;
        
        toDiagonalAngleComple=toDiagonalAngle+degrees2;
        toDiagonalAngleComple2=toDiagonalAngle2+degrees2;
        
        finalHeight=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple2)));
        finalWidth=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple)));
        
        t1 = CGAffineTransformMakeTranslation(width*sinf(DEGREES_TO_RADIANS(degrees2))+height*cosf(DEGREES_TO_RADIANS(degrees2)), height*sinf(DEGREES_TO_RADIANS(degrees2)));
    }
    else if(degrees>=-90&&degrees<0){
        
        float degrees2 = degrees-90;
        float degreesabs = ABS(degrees);
        
        toDiagonalAngleComple=toDiagonalAngle+degrees2;
        toDiagonalAngleComple2=toDiagonalAngle2+degrees2;
        
        finalHeight=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple2)));
        finalWidth=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple)));
        
        t1 = CGAffineTransformMakeTranslation(0, width*sinf(DEGREES_TO_RADIANS(degreesabs)));
        
    }
    else if(degrees>=-180&&degrees<-90){
        
        float degreesabs = ABS(degrees);
        float degreesplus = degreesabs-90;
        
        toDiagonalAngleComple=toDiagonalAngle+degrees;
        toDiagonalAngleComple2=toDiagonalAngle2+degrees;
        
        finalHeight=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple)));
        finalWidth=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple2)));
        
        t1 = CGAffineTransformMakeTranslation(width*sinf(DEGREES_TO_RADIANS(degreesplus)), height*sinf(DEGREES_TO_RADIANS(degreesplus))+width*cosf(DEGREES_TO_RADIANS(degreesplus)));
        
    }
    
    
    // Rotate transformation
    t2 = CGAffineTransformRotate(t1, DEGREES_TO_RADIANS(degrees));
    //t2 = CGAffineTransformRotate(t1, -90);
    
    
    // Step 3
    // Set the appropriate render sizes and rotational transforms
    if (!self.mutableVideoComposition) {
        
        // Create a new video composition
        self.mutableVideoComposition = [AVMutableVideoComposition videoComposition];
        // self.mutableVideoComposition.renderSize = CGSizeMake(assetVideoTrack.naturalSize.height,assetVideoTrack.naturalSize.width);
        self.mutableVideoComposition.renderSize = CGSizeMake(finalWidth,finalHeight);
        
        self.mutableVideoComposition.frameDuration = CMTimeMake(1,30);
        
        // The rotate transform is set on a layer instruction
        instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [self.mutableComposition duration]);
        layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:(self.mutableComposition.tracks)[0]];
        [layerInstruction setTransform:t2 atTime:kCMTimeZero];
        
    } else {
        
        self.mutableVideoComposition.renderSize = CGSizeMake(self.mutableVideoComposition.renderSize.height, self.mutableVideoComposition.renderSize.width);
        
        // Extract the existing layer instruction on the mutableVideoComposition
        instruction = (self.mutableVideoComposition.instructions)[0];
        layerInstruction = (instruction.layerInstructions)[0];
        
        // Check if a transform already exists on this layer instruction, this is done to add the current transform on top of previous edits
        CGAffineTransform existingTransform;
        
        if (![layerInstruction getTransformRampForTime:[self.mutableComposition duration] startTransform:&existingTransform endTransform:NULL timeRange:NULL]) {
            [layerInstruction setTransform:t2 atTime:kCMTimeZero];
        } else {
            // Note: the point of origin for rotation is the upper left corner of the composition, t3 is to compensate for origin
            CGAffineTransform t3 = CGAffineTransformMakeTranslation(-1*assetVideoTrack.naturalSize.height/2, 0.0);
            CGAffineTransform newTransform = CGAffineTransformConcat(existingTransform, CGAffineTransformConcat(t2, t3));
            [layerInstruction setTransform:newTransform atTime:kCMTimeZero];
        }
        
    }
    
    
    // Step 4
    // Add the transform instructions to the video composition
    instruction.layerInstructions = @[layerInstruction];
    self.mutableVideoComposition.instructions = @[instruction];
    
    
    // Step 5
    // Notify AVSEViewController about rotation operation completion
    // [[NSNotificationCenter defaultCenter] postNotificationName:AVSEEditCommandCompletionNotification object:self];
    
    [self performWithAssetExport];
}

- (void)moveToHomeScreen
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    HomeController *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"HomeController"];
    GoToVC.strVideoURL = _strVideoUrl;
    GoToVC.strVideoDuration = _strVideoDuration;
    GoToVC.strVideoName = _strVideoName;
    GoToVC.durationForSliderHome = durationforSlider;
    [self.navigationController pushViewController:GoToVC animated:YES];
}

- (void)performWithAssetExport
{
    // Step 1
    // Create an outputURL to which the exported movie will be saved
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *outputURL = paths[0];
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
    outputURL = [outputURL stringByAppendingPathComponent:@"output.mov"];
    // Remove Existing File
    [manager removeItemAtPath:outputURL error:nil];
    
    
    // Step 2
    // Create an export session with the composition and write the exported movie to the photo library
    self.exportSession = [[AVAssetExportSession alloc] initWithAsset:[self.mutableComposition copy] presetName:AVAssetExportPreset1280x720];
    
    self.exportSession.videoComposition = self.mutableVideoComposition;
    self.exportSession.audioMix = self.mutableAudioMix;
    self.exportSession.outputURL = [NSURL fileURLWithPath:outputURL];
    self.exportSession.outputFileType=AVFileTypeQuickTimeMovie;
    
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [self.exportSession exportAsynchronouslyWithCompletionHandler:^(void){
        
        switch (self.exportSession.status) {
            case AVAssetExportSessionStatusCompleted:
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"Backhome" object:nil];
                
                _strVideoUrl = _exportSession.outputURL.path;
                
                NSURL *sourceMovieURL = [NSURL fileURLWithPath:_strVideoUrl];
                AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:sourceMovieURL options:nil];
                
                CMTime duration = sourceAsset.duration;
                float seconds = CMTimeGetSeconds(duration);
                NSLog(@"duration: %.2f", seconds);
                
                int totalSeconds = (int)ceilf(seconds);
                NSString *strTotalSeconds = [self timeFormatted:totalSeconds];
                
                _strVideoDuration = strTotalSeconds;
                _strVideoName = @"Youtube Video";
                
                [self performSelectorOnMainThread:@selector(moveToHomeScreen) withObject:nil waitUntilDone:2.0];
                
                dispatch_async(dispatch_get_main_queue(), ^{

                    /*
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                    HomeController *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"HomeController"];
                    GoToVC.strVideoURL = _strVideoUrl;
                    GoToVC.strVideoDuration = _strVideoDuration;
                    GoToVC.strVideoName = _strVideoName;
                    [self.navigationController pushViewController:GoToVC animated:YES];*/
                    
                });
                                
            }
                
                break;
            case AVAssetExportSessionStatusFailed:
                NSLog(@"Failed:%@",self.exportSession.error);
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Canceled:%@",self.exportSession.error);
                break;
            default:
                break;
        }
    }];
    
}


//Nimit
- (NSString *)timeFormatted:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

- (IBAction)btnRecordClicked:(id)sender {
    
        
    if (![_txtYouTubeLink.text isEqualToString:@""]) {
        [self WSGetVideoURL];
    }
    else{
        if ([_strVideoUrl isEqualToString:@""] || _strVideoUrl == nil) {
            
            [Global showToast:@"Please add link or select the video"];
            return;
        }
        /*
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        HomeController *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"HomeController"];
        GoToVC.strVideoURL = _strVideoUrl;
        GoToVC.strVideoDuration = _strVideoDuration;
        GoToVC.strVideoName = _strVideoName;
        GoToVC.durationForSliderHome = durationforSlider;

        [self.navigationController pushViewController:GoToVC animated:YES];
         */
        [self moveToHomeScreen];
    }
   
}


-(void)getDirectoryContents{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSError * error;
    NSArray * directoryContents =  [[NSFileManager defaultManager]
                                    contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    NSLog(@"directoryContents ====== %@",directoryContents);
    
    for (int i=0; i<directoryContents.count; i++) {
        [self removeItem:[directoryContents objectAtIndex:i]];
    }
}


- (void)removeItem:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
        NSLog(@"%@ removed successfully!",filename);
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

@end
