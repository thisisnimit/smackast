//
//  CMMediaPickerController.h
//

@import UIKit;
@import AssetsLibrary;

typedef NS_OPTIONS(NSInteger, CMMediaPickerControllerMediaType)
{
    CMMediaPickerControllerMediaTypeImage = 1 << 0,
    CMMediaPickerControllerMediaTypeVideo = 1 << 1
};

typedef NS_OPTIONS(NSInteger, CMMediaPickerControllerSourceType)
{
    CMMediaPickerControllerSourceTypePhotoLibrary       = 1 << 0,
    CMMediaPickerControllerSourceTypeCamera             = 1 << 1,
    CMMediaPickerControllerSourceTypeSavedPhotosAlbum   = 1 << 2,
    CMMediaPickerControllerSourceTypeLastPhotoTaken     = 1 << 3
};

@class CMMediaPickerController;

@protocol CMMediaPickerControllerDelegate <NSObject>

@optional
- (void)CMMediaPickerController:(CMMediaPickerController *)mediaPickerController didFinishPickingAsset:(ALAsset *)asset error:(NSError *)error;
- (void)CMMediaPickerControllerDidCancel:(CMMediaPickerController *)mediaPickerController;
@end

@interface CMMediaPickerController: NSObject

@property (nonatomic, strong, readonly) UIImagePickerController *imagePickerController;
@property (nonatomic, assign) CMMediaPickerControllerMediaType mediaType;
@property (nonatomic, assign) CMMediaPickerControllerSourceType sourceType;
@property (nonatomic, assign) NSTimeInterval videoMaximumDuration;
@property (nonatomic, assign) UIImagePickerControllerQualityType videoQualityType;

@property (nonatomic, copy) Class popoverControllerClass;

@property (nonatomic, weak) UIViewController<CMMediaPickerControllerDelegate> *delegate;

@property (nonatomic) BOOL allowsEditing;
@property (nonatomic, assign) UIImagePickerControllerCameraCaptureMode cameraCaptureMode;
@property (nonatomic, assign) UIImagePickerControllerCameraDevice cameraDevice;
@property (nonatomic, assign) UIImagePickerControllerCameraFlashMode cameraFlashMode;
@property (nonatomic, retain) UIView *cameraOverlayView;
@property (nonatomic, assign) CGAffineTransform cameraViewTransform;
@property (nonatomic, assign) BOOL showsCameraControls;

- (void)show;
- (void)dismiss;
- (BOOL)startVideoCapture;
- (void)stopVideoCapture;
- (void)takePicture;

@end

