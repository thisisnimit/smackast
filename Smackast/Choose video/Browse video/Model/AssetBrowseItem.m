//
//  AssetBrowseItem.m
//  VideoPicker
//
//  Created by MAYUR PATEL on 11/8/16.
//  Copyright © 2016 MAYUR PATEL. All rights reserved.
//

#import "AssetBrowseItem.h"

@implementation AssetBrowseItem

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init])
    {
        self.videoTitle = dictionary[@"VideoTitle"];
        self.videoURL = dictionary[@"VideoUrl"];
        self.videoImage = dictionary[@"VideoImage"];
        self.videoSize = [dictionary[@"VideoSize"] longLongValue];
        self.videoLength = [self timeFormatted:[dictionary[@"VideoLength"] doubleValue]];
        self.videoDuration = [dictionary[@"VideoLength"] floatValue];
    }
    return self;
}

- (NSString *)timeFormatted:(double)totalSeconds
{
    NSTimeInterval timeInterval = totalSeconds;
    long seconds = lroundf(timeInterval); // Modulo (%) operator below needs int or long
    int hour = 0;
    int minute = seconds/60.0f;
    int second = seconds % 60;
    if (minute > 59)
    {
        hour = minute/60;
        minute = minute%60;
        return [NSString stringWithFormat:@"%02d:%02d:%02d", hour, minute, second];
    }
    else
    {
        return [NSString stringWithFormat:@"%02d:%02d", minute, second];
    }
}

@end
