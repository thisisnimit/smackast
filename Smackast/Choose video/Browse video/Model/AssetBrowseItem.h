//
//  AssetBrowseItem.h
//  VideoPicker
//
//  Created by MAYUR PATEL on 11/8/16.
//  Copyright © 2016 MAYUR PATEL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "videoImage.h"
@interface AssetBrowseItem : NSObject
@property (strong, nonatomic) NSString *videoTitle;
@property (strong, nonatomic) NSURL *videoURL;
@property (strong, nonatomic) NSString *videoLength;
@property (strong, nonatomic) videoImage *videoImage;
@property (readwrite, nonatomic) long long videoSize;
@property (nonatomic) CGFloat videoDuration;

- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
