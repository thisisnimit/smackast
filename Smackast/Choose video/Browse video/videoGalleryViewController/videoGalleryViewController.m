//
//  videoGalleryCollectionViewController.m
//  VideoPicker
//
//  Created by MAYUR PATEL on 11/8/16.
//  Copyright © 2016 MAYUR PATEL. All rights reserved.
//

#import "videoGalleryViewController.h"
#import "CCellVideo.h"
#import "AssetBrowseItem.h"

@import AssetsLibrary;
@interface videoGalleryViewController ()
@end

@implementation videoGalleryViewController

static NSString * const reuseIdentifier = @"CCell";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.arrVideoList = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.activityIndicator setHidden:FALSE];
    [self.activityIndicator startAnimating];
    [self updateAssetsLibrary];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Show Video List Methods

- (void)updateAssetsLibrary
{
    ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
    
    [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop)
     {
         if (group)
         {
             [group setAssetsFilter:[ALAssetsFilter allVideos]];
             [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop)
              {
                  if (asset)
                  {
                      ALAssetRepresentation *defaultRepresentation = [asset defaultRepresentation];
                      
                      NSMutableDictionary *dictVideoData = [[NSMutableDictionary alloc] init];
                      [dictVideoData setValue:[defaultRepresentation filename] forKey:@"VideoTitle"];
                      [dictVideoData setValue:[NSNumber numberWithDouble: [[asset valueForProperty:ALAssetPropertyDuration] doubleValue]] forKey:@"VideoLength"];
                      [dictVideoData setValue:[defaultRepresentation url] forKey:@"VideoUrl"];
                      [dictVideoData setValue:[UIImage imageWithCGImage:[defaultRepresentation fullResolutionImage]] forKey:@"VideoImage"];
                      [dictVideoData setValue:[NSNumber numberWithLongLong:[defaultRepresentation size]] forKey:@"VideoSize"];
                      
                      AssetBrowseItem *item = [[AssetBrowseItem alloc] initWithDictionary:dictVideoData];
                      [self.arrVideoList addObject:item];
                  }
              } ];
         }
         // group == nil signals we are done iterating.
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 //Stop Loader - Reload data
                 NSLog(@"Video List:%@",self.arrVideoList);
                 [self.collectionView reloadData];
                 [self.activityIndicator setHidden:TRUE];
                 [self.activityIndicator stopAnimating];
             });
         }
     }failureBlock:^(NSError *error)
     {
         NSLog(@"error enumerating AssetLibrary groups %@\n", error);
     }];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if([self.arrVideoList count]>0)
    {
        return [self.arrVideoList count];
    }
    else
    {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CCellVideo *cell = (CCellVideo*)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    AssetBrowseItem *item = self.arrVideoList[indexPath.row];
    cell.imgViewVideo.image = item.videoImage;
    cell.lblLength.text = item.videoLength;
    // Configure the cell
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1.0f;
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)cv layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat targetWidth = ([UIScreen mainScreen].bounds.size.width - 3)/4;
    return CGSizeMake(floorf(targetWidth), floorf(targetWidth));
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(selectedVideoURL:)])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.delegate selectedVideoURL:self.arrVideoList[indexPath.row]];
    }
}

@end
