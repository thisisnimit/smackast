//
//  videoGalleryCollectionViewController.h
//  VideoPicker
//
//  Created by MAYUR PATEL on 11/8/16.
//  Copyright © 2016 MAYUR PATEL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetBrowseItem.h"

@protocol VideoGalleryDelegate <NSObject>
@optional
- (void)selectedVideoURL:(AssetBrowseItem*)item;
@end

@interface videoGalleryViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *arrVideoList;
@property (strong, nonatomic) id<VideoGalleryDelegate>delegate;
@end
