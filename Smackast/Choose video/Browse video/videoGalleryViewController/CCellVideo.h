//
//  CCellVideo.h
//  VideoPicker
//
//  Created by MAYUR PATEL on 11/8/16.
//  Copyright © 2016 MAYUR PATEL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCellVideo : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewVideo;
@property (weak, nonatomic) IBOutlet UILabel *lblLength;

@end
