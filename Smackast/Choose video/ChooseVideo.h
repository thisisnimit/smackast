//
//  ChooseVideo.h
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeController.h"
#import "videoGalleryViewController.h"
#import "CMMediaPickerController.h"

#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>

#import "ProfileController.h"
#import "JVFloatLabeledTextField.h"

#import <QuartzCore/QuartzCore.h>


@interface ChooseVideo : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,VideoGalleryDelegate>{
    NSMutableArray *arrSites;
    NSUserDefaults *sharedUserDefaults;
    CGFloat durationforSlider;
}


@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtYouTubeLink;
@property (strong, nonatomic) NSString *strVideoUrl;
@property (strong, nonatomic) NSString *strVideoDuration;
@property (strong, nonatomic) NSString *strVideoName;
@property (weak, nonatomic) IBOutlet UIButton *btnRecordVideo;

@property (weak, nonatomic) IBOutlet UILabel *lblVideoName;

- (IBAction)btnBrowseClicked:(id)sender;
- (IBAction)btnRecordClicked:(id)sender;


// experiment

@property AVMutableComposition *mutableComposition;
@property AVMutableVideoComposition *mutableVideoComposition;
@property AVMutableAudioMix *mutableAudioMix;
@property AVAssetExportSession *exportSession;
- (void)performWithAsset : (NSURL *)moviename;
- (IBAction)btnProfileClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnBrowse;

@end
