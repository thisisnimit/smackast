//
//  HomeController.h
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadVideo.h"
#import "SAVideoRangeSlider.h"
#import "TTMCaptureManager.h"
#import <AVFoundation/AVFoundation.h>
#import "UploadVideo.h"
#import <AudioToolbox/AudioServices.h>
#import "MBCircularProgressBarView.h"
#import "KVTimer.h"
#import "Global.h"

@interface HomeController : UIViewController<AVAudioRecorderDelegate, AVAudioPlayerDelegate, KVTimerDelegate>{
    float oldX, oldY;
    BOOL dragging;
    
    NSURL *outputFileURL;
    BOOL isBeingRecorded;
    
}
@property (weak, nonatomic) IBOutlet UIView *viewVideo;
@property (weak, nonatomic) IBOutlet UIView *viewCamera;
@property (weak, nonatomic) IBOutlet UISlider *sliderTime;

@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;
- (IBAction)btnBackClicked:(id)sender;

@property (strong, nonatomic) NSString *strVideoURL;
@property (strong, nonatomic) NSString *strVideoName;
@property (strong, nonatomic) NSString *strVideoDuration;

@property (weak, nonatomic) IBOutlet UIButton *btnRecord;
@property (weak, nonatomic) IBOutlet UIView *viewRecordButton;

@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewCameraHeight;

@property (weak, nonatomic) IBOutlet UIButton *btnVideo;

// crop

@property (nonatomic) CGFloat startTime;
@property (nonatomic) CGFloat stopTime;

@property (strong, nonatomic) NSString *tmpVideoPath;
@property (strong, nonatomic) AVAssetExportSession *exportSession;

// experiment

@property AVMutableComposition *mutableComposition;
@property AVMutableVideoComposition *mutableVideoComposition;
@property AVMutableAudioMix *mutableAudioMix;
//@property AVAssetExportSession *exportSession;
- (void)performWithAsset : (NSURL *)moviename;

- (IBAction)btnAudioClicked:(UIButton *)sender;
- (IBAction)btnVideoClicked:(UIButton *)sender;


@property (strong, nonatomic) NSString *strCroppedVideoPath;
@property (strong, nonatomic) NSString *strRecordedVideoPath;
@property (strong, nonatomic) NSString *strAudioPath;

@property (nonatomic) CGFloat durationForSliderHome;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *myProgressBar;

//
@property(nonatomic,retain)AVAsset* firstAsset;
@property(nonatomic,retain)AVAsset* secondAsset;
@property(nonatomic,retain)AVAsset* audioAsset;

@property (weak, nonatomic) IBOutlet KVTimer *countdownTimer;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@end
