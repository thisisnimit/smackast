//
//  HomeController.m
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "HomeController.h"
@import MediaPlayer;

@interface HomeController ()<TTMCaptureManagerDelegate>
{
    BOOL isNeededToSave;
    AVAudioPlayer *player;
    AVAudioRecorder *recorder;
}
@property (nonatomic, strong) TTMCaptureManager *captureManager;

@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;

@end

@implementation HomeController
@synthesize firstAsset,secondAsset,audioAsset;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"URL ::: %@\nDuration ::: %@\nName ::: %@",_strVideoURL,_strVideoDuration,_strVideoName);
    
    [self.view setUserInteractionEnabled:YES];
    dragging = NO;
    [self.myProgressBar setHidden:YES];

    //_constraintViewCameraHeight.constant = _viewVideo.frame.size.height;

    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error: nil];
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,
                             sizeof (audioRouteOverride),&audioRouteOverride);
    
    NSString *tempDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    self.tmpVideoPath = [tempDir stringByAppendingPathComponent:@"MyCroppedVideo.mov"];
    
    _lblEndTime.text = _strVideoDuration;
    
    NSURL *url = [NSURL fileURLWithPath:_strVideoURL];
    
    [self moviePlayer:url];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];

    [_btnBack setEnabled:false];
    //[self getDirectoryContents];
}



-(void)getDirectoryContents{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    //NSString *documentsDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *documentsDirectory = ([paths count] > 0) ? [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] : nil;

    NSError * error;
    NSArray * directoryContents =  [[NSFileManager defaultManager]
                                    contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    NSLog(@"directoryContents ====== %@",directoryContents);
    
    for (int i=0; i<directoryContents.count; i++) {
        [self removeItem:[directoryContents objectAtIndex:i]];
    }
}

- (void)removeItem:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = ([paths count] > 0) ? [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] : nil;
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
        NSLog(@"%@ removed successfully!",filename);
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_sliderTime setMaximumValue:_durationForSliderHome];
    [_sliderTime setMinimumValue:0.0];

    [self setUpAudio];
    
    self.countdownTimer.delegate = self;
    [self.countdownTimer setShowTimerLabel:YES];
    [self.countdownTimer setMaxTime:3 minTime:3];
    self.countdownTimer.interval = KVIntervalSecond;
    [self.countdownTimer setKofString:@"Second"];
    
    
    KVStyle *circlestyle = [KVStyle initWithFillColor:[UIColor clearColor]
                                    strokeColor:[UIColor clearColor]
                                      lineWidth:5
                                         radius:50];
    KVStyle *linestyle = [KVStyle initWithFillColor:[UIColor clearColor]
                                          strokeColor:[UIColor colorWithHexString:@"0084FF"]
                                            lineWidth:5
                                               radius:50];
    
    KVStyle *pintyle = [KVStyle initWithFillColor:[UIColor colorWithHexString:@"0084FF"]
                                        strokeColor:[UIColor colorWithHexString:@"0084FF"]
                                          lineWidth:5
                                             radius:0];

    [self.countdownTimer setStyleLine:linestyle];
    [self.countdownTimer setStylePin:pintyle];
    [self.countdownTimer setStyleCircle:circlestyle];
    [self.countdownTimer startTimer:true];
    
}


#pragma mark - KVTimer delegate

- (void)getTimer:(NSString *)time{
    NSLog(@"Time: %@", time);
}

- (void)endTime{
    [_btnBack setEnabled:true];
    [self.countdownTimer setHidden:YES];
    [self playVideo];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setUpCamera];
}

-(void)setUpAudio{
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyRecordedAudio.m4a",
                               nil];
    outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    NSLog(@"Output Audio : %@", outputFileURL);
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    [recordSetting setValue :[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    [recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
    [recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.moviePlayer stop];
    [self pauseVideo];
}


-(void)setUpCamera{
    self.captureManager = [[TTMCaptureManager alloc] initWithPreviewView:self.viewCamera
                                                     preferredCameraType:CameraTypeFront
                                                              outputMode:OutputModeVideoData];
    self.captureManager.delegate = self;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [self.captureManager updateOrientationWithPreviewView:self.viewCamera];
}


- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:player];
    
    NSLog(@"Video ended!!!");
    
    if (dragging) {
        NSLog(@"Processing afrer video ended!!!");
        [self pauseVideo];
        dragging = NO;
        self.stopTime = self.moviePlayer.currentPlaybackTime;
        
        if (_btnVideo.isSelected) {
            //stop audio recording...
            [self stopAudioRecording];
        }
        else{
            isNeededToSave = YES;
            [self.captureManager stopRecording];
        }
    }
    else{
        [self playVideo];
    }
    [_btnPlay setSelected:NO];
    //[_btnPlay setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [_btnPlay setImage:nil forState:UIControlStateNormal];

    _sliderTime.value = 0.0;
    self.moviePlayer.currentPlaybackTime = 0.0;

}


#pragma mark -
#pragma mark PlayVideo

- (void)moviePlayer:(NSURL*)url
{
    NSLog(@"Final URL To Play Video :-> %@",[url absoluteString]);
    
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    self.moviePlayer.movieSourceType = MPMediaTypeAnyVideo;
    self.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    self.moviePlayer.repeatMode = MPMovieRepeatModeNone;
    self.moviePlayer.allowsAirPlay = NO;
    self.moviePlayer.shouldAutoplay = NO;
    self.moviePlayer.controlStyle = MPMovieControlStyleNone;
    self.moviePlayer.view.frame = self.viewVideo.bounds;
    self.moviePlayer.view.autoresizingMask = (UIViewAutoresizing)(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.viewVideo addSubview:self.moviePlayer.view];
    [self.moviePlayer prepareToPlay];
    [self addPlayPauseButton];
}

-(void)addPlayPauseButton{
    
    [_btnPlay addTarget:self
                 action:@selector(btnMyPlayPauseClicked:)
       forControlEvents:UIControlEventTouchUpInside];
    //[_btnPlay setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [_btnPlay setImage:nil forState:UIControlStateNormal];

    [_viewVideo addSubview:_btnPlay];
}

-(void) btnMyPlayPauseClicked:(UIButton*)sender
{
    NSLog(@"you clicked on button %ld", (long)sender.tag);
    
    sender.selected = !sender.selected;
    
    if (sender.isSelected) {
        [self playVideo];
    }
    else{
        [self pauseVideo];
    }
}

-(void)playVideo{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_moviePlayer];
    [_btnPlay setSelected:YES];
    [_btnPlay setImage:nil forState:UIControlStateNormal];
    [self.moviePlayer play];
}

-(void)pauseVideo{
    [_btnPlay setSelected:NO];
    //[_btnPlay setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [_btnPlay setImage:nil forState:UIControlStateNormal];

    [self.moviePlayer pause];
}

- (void)updateSlider {
    
    _sliderTime.value = self.moviePlayer.currentPlaybackTime;
    
    int totalSeconds = (int)ceilf(self.moviePlayer.currentPlaybackTime);
    NSString *strTotalSeconds = [self timeFormatted:totalSeconds];
    _lblStartTime.text = strTotalSeconds;

    //_progressTime.progress = self.moviePlayer.playableDuration/totalSeconds;
    
    NSLog(@"%ld",(long)self.moviePlayer.playbackState);
    
    if (_btnPlay.isSelected) {
        if (_moviePlayer.currentPlaybackTime < _moviePlayer.playableDuration) {
            [self.moviePlayer play];
        }
    }
    if(self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying){
        NSLog(@"MPMoviePlaybackStatePlaying");
        [_btnPlay setImage:nil forState:UIControlStateNormal];
    }
    
}

//Nimit
- (NSString *)timeFormatted:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours == 0) {
        return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    }

    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

- (IBAction)sliderTimeClicked:(id)sender {
    NSLog(@"Slider time : %f",_sliderTime.value);
    self.moviePlayer.currentPlaybackTime = _sliderTime.value;
}

-(void)startAudioRecording{
    
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    isBeingRecorded = YES;
    [recorder record];
}

-(void)pauseAudioRecording{
    [recorder pause];
}

-(void)stopAudioRecording{
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    
    [self cropVideo];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch= [touches anyObject];
    if ([touch view] == _viewRecordButton)
    {
        
        if(self.moviePlayer.playbackState != MPMoviePlaybackStatePlaying){
            [Global showToast:@"Play video to start recording!"];
            return;
        }
        
        NSLog(@"touchesBegan");
        dragging = YES;
        NSLog(@"Crop Start Time : %f",self.moviePlayer.currentPlaybackTime);
        self.startTime = self.moviePlayer.currentPlaybackTime;
        
        if (_btnVideo.isSelected) {
            // start audio recording!!!
            NSLog(@"Audio Recording Started...");
            [self startAudioRecording];
        }
        else{
            NSLog(@"Video Recording Started...");
            [self.captureManager startRecording];
        }
        
        [self.myProgressBar setHidden:NO];
        self.myProgressBar.value = 100.0f;
        if (_durationForSliderHome - _startTime > 30.0) {
            [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(stopVideoFromEverything) userInfo:nil repeats:YES];
            [UIView animateWithDuration:30.f animations:^{
                self.myProgressBar.value = 100.f - self.myProgressBar.value;
            }];
        }
        else{
            CGFloat totalTime = _durationForSliderHome - _startTime;
            [UIView animateWithDuration:totalTime animations:^{
                self.myProgressBar.value = 100.f - self.myProgressBar.value;
            }];
        }
        
        //[self flashOn:_btnRecord];
        //[self glowUIButton:_btnRecord];
    }
}

- (void)stopVideoFromEverything {
    NSLog(@"30 seconds over");
    if (!dragging) {
        return;
    }
    dragging = NO;
    NSLog(@"Crop End Time : %f",self.moviePlayer.currentPlaybackTime);
    self.stopTime = self.moviePlayer.currentPlaybackTime;
    [self.moviePlayer stop];
    
    if (_btnVideo.isSelected) {
        // stop audio recording!!!
        [self stopAudioRecording];
        
    }
    else{
        isNeededToSave = YES;
        [self.captureManager stopRecording];
    }
    [self.myProgressBar setHidden:YES];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (dragging)
    {
        NSLog(@"touchesEnded");
        dragging = NO;
        NSLog(@"Crop End Time : %f",self.moviePlayer.currentPlaybackTime);
        self.stopTime = self.moviePlayer.currentPlaybackTime;
        [self.moviePlayer stop];

        if (_btnVideo.isSelected) {
            // stop audio recording!!!
            [self stopAudioRecording];

        }
        else{
            isNeededToSave = YES;
            [self.captureManager stopRecording];
        }
        [self.myProgressBar setHidden:YES];
        
        //[self flashOff:_btnRecord];
        //[self removeGlowUIButton:_btnRecord];

    }
}

-(void) glowUIButton:(UIButton *)inputButton
{
    //add blink effect
    CALayer *viewLayer = inputButton.layer;
    viewLayer.shadowOffset = CGSizeMake(0,0);
    CGFloat radius = CGRectGetWidth(inputButton.bounds)/2.0;
    viewLayer.shadowColor = [[UIColor whiteColor] CGColor];
    viewLayer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(-7,-5.5, 2.5 * (radius), 2.5 * radius) cornerRadius:radius].CGPath;
    viewLayer.shadowRadius = 5.0f;
    viewLayer.shadowOpacity = 1.0f;
    
    //Let's animate it while we're at it.
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
    animation.duration =0.7;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animation.fromValue = [NSNumber numberWithFloat:1.0];
    animation.toValue = [NSNumber numberWithFloat:0.0];
    animation.autoreverses = YES;
    animation.repeatCount = 1.0 / 0.0;
    [viewLayer addAnimation:animation forKey:@"shadowOpacity"];
    
}

-(void)removeGlowUIButton:(UIButton *)inputButton
{
    CALayer *viewLayer = inputButton.layer;
    viewLayer.shadowColor = [[UIColor clearColor] CGColor];
    [viewLayer removeAnimationForKey:@"shadowOpacity"];
}

- (void)flashOff:(UIView *)v
{
    [UIView animateWithDuration:.05 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^ {
        v.alpha = .01;  //don't animate alpha to 0, otherwise you won't be able to interact with it
    } completion:^(BOOL finished) {
        [self flashOn:v];
    }];
}

- (void)flashOn:(UIView *)v
{
    [UIView animateWithDuration:.05 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^ {
        v.alpha = 1;
    } completion:^(BOOL finished) {
        [self flashOff:v];
    }];
}

// =============================================================================
#pragma mark - AVCaptureManagerDeleagte

- (void)didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL error:(NSError *)error {
    
    //LOG_CURRENT_METHOD;
    
    if (error) {
        NSLog(@"error:%@", error);
        return;
    }
    
    if (!isNeededToSave) {
        return;
    }
    
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [self saveRecordedFile:outputFileURL];
}

- (void)saveRecordedFile:(NSURL *)recordedFile {
    
    [self performWithAsset:recordedFile];
}

- (void)performWithAsset : (NSURL *)moviename
{
    
    self.mutableComposition=nil;
    self.mutableVideoComposition=nil;
    self.mutableAudioMix=nil;
    
    AVAsset *asset = [[AVURLAsset alloc] initWithURL:moviename options:nil];
    
    AVMutableVideoCompositionInstruction *instruction = nil;
    AVMutableVideoCompositionLayerInstruction *layerInstruction = nil;
    CGAffineTransform t1;
    CGAffineTransform t2;
    
    AVAssetTrack *assetVideoTrack = nil;
    AVAssetTrack *assetAudioTrack = nil;
    // Check if the asset contains video and audio tracks
    if ([[asset tracksWithMediaType:AVMediaTypeVideo] count] != 0) {
        assetVideoTrack = [asset tracksWithMediaType:AVMediaTypeVideo][0];
    }
    if ([[asset tracksWithMediaType:AVMediaTypeAudio] count] != 0) {
        assetAudioTrack = [asset tracksWithMediaType:AVMediaTypeAudio][0];
    }
    
    CMTime insertionPoint = kCMTimeZero;
    NSError *error = nil;
    
    
    // Step 1
    // Create a composition with the given asset and insert audio and video tracks into it from the asset
    if (!self.mutableComposition) {
        
        // Check whether a composition has already been created, i.e, some other tool has already been applied
        // Create a new composition
        self.mutableComposition = [AVMutableComposition composition];
        
        // Insert the video and audio tracks from AVAsset
        if (assetVideoTrack != nil) {
            AVMutableCompositionTrack *compositionVideoTrack = [self.mutableComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
            [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [asset duration]) ofTrack:assetVideoTrack atTime:insertionPoint error:&error];
        }
        if (assetAudioTrack != nil) {
            AVMutableCompositionTrack *compositionAudioTrack = [self.mutableComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [asset duration]) ofTrack:assetAudioTrack atTime:insertionPoint error:&error];
        }
        
    }
    
    
    // Step 2
    // Translate the composition to compensate the movement caused by rotation (since rotation would cause it to move out of frame)
    t1 = CGAffineTransformMakeTranslation(assetVideoTrack.naturalSize.height, 0.0);
    float width=assetVideoTrack.naturalSize.width;
    float height=assetVideoTrack.naturalSize.height;
    float toDiagonal=sqrt(width*width+height*height);
    //float toDiagonalAngle = radiansToDegrees(acosf(width/toDiagonal));
    float toDiagonalAngle = RADIANS_TO_DEGREES(acosf(width/toDiagonal));
    float toDiagonalAngle2=90-RADIANS_TO_DEGREES(acosf(width/toDiagonal));
    
    float toDiagonalAngleComple;
    float toDiagonalAngleComple2;
    float finalHeight = 0.0;
    float finalWidth = 0.0;
    
    float degrees=90;
    
    if(degrees>=0&&degrees<=90){
        
        toDiagonalAngleComple=toDiagonalAngle+degrees;
        toDiagonalAngleComple2=toDiagonalAngle2+degrees;
        
        finalHeight=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple)));
        finalWidth=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple2)));
        
        t1 = CGAffineTransformMakeTranslation(height*sinf(DEGREES_TO_RADIANS(degrees)), 0.0);
    }
    else if(degrees>90&&degrees<=180){
        
        
        float degrees2 = degrees-90;
        
        toDiagonalAngleComple=toDiagonalAngle+degrees2;
        toDiagonalAngleComple2=toDiagonalAngle2+degrees2;
        
        finalHeight=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple2)));
        finalWidth=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple)));
        
        t1 = CGAffineTransformMakeTranslation(width*sinf(DEGREES_TO_RADIANS(degrees2))+height*cosf(DEGREES_TO_RADIANS(degrees2)), height*sinf(DEGREES_TO_RADIANS(degrees2)));
    }
    else if(degrees>=-90&&degrees<0){
        
        float degrees2 = degrees-90;
        float degreesabs = ABS(degrees);
        
        toDiagonalAngleComple=toDiagonalAngle+degrees2;
        toDiagonalAngleComple2=toDiagonalAngle2+degrees2;
        
        finalHeight=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple2)));
        finalWidth=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple)));
        
        t1 = CGAffineTransformMakeTranslation(0, width*sinf(DEGREES_TO_RADIANS(degreesabs)));
        
    }
    else if(degrees>=-180&&degrees<-90){
        
        float degreesabs = ABS(degrees);
        float degreesplus = degreesabs-90;
        
        toDiagonalAngleComple=toDiagonalAngle+degrees;
        toDiagonalAngleComple2=toDiagonalAngle2+degrees;
        
        finalHeight=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple)));
        finalWidth=ABS(toDiagonal*sinf(DEGREES_TO_RADIANS(toDiagonalAngleComple2)));
        
        t1 = CGAffineTransformMakeTranslation(width*sinf(DEGREES_TO_RADIANS(degreesplus)), height*sinf(DEGREES_TO_RADIANS(degreesplus))+width*cosf(DEGREES_TO_RADIANS(degreesplus)));
        
    }
    
    
    // Rotate transformation
    t2 = CGAffineTransformRotate(t1, DEGREES_TO_RADIANS(degrees));
    //t2 = CGAffineTransformRotate(t1, -90);
    
    
    // Step 3
    // Set the appropriate render sizes and rotational transforms
    if (!self.mutableVideoComposition) {
        
        // Create a new video composition
        self.mutableVideoComposition = [AVMutableVideoComposition videoComposition];
        // self.mutableVideoComposition.renderSize = CGSizeMake(assetVideoTrack.naturalSize.height,assetVideoTrack.naturalSize.width);
        self.mutableVideoComposition.renderSize = CGSizeMake(finalWidth,finalHeight);
        
        self.mutableVideoComposition.frameDuration = CMTimeMake(1,30);
        
        // The rotate transform is set on a layer instruction
        instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [self.mutableComposition duration]);
        layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:(self.mutableComposition.tracks)[0]];
        [layerInstruction setTransform:t2 atTime:kCMTimeZero];
        
    } else {
        
        self.mutableVideoComposition.renderSize = CGSizeMake(self.mutableVideoComposition.renderSize.height, self.mutableVideoComposition.renderSize.width);
        
        // Extract the existing layer instruction on the mutableVideoComposition
        instruction = (self.mutableVideoComposition.instructions)[0];
        layerInstruction = (instruction.layerInstructions)[0];
        
        // Check if a transform already exists on this layer instruction, this is done to add the current transform on top of previous edits
        CGAffineTransform existingTransform;
        
        if (![layerInstruction getTransformRampForTime:[self.mutableComposition duration] startTransform:&existingTransform endTransform:NULL timeRange:NULL]) {
            [layerInstruction setTransform:t2 atTime:kCMTimeZero];
        } else {
            // Note: the point of origin for rotation is the upper left corner of the composition, t3 is to compensate for origin
            CGAffineTransform t3 = CGAffineTransformMakeTranslation(-1*assetVideoTrack.naturalSize.height/2, 0.0);
            CGAffineTransform newTransform = CGAffineTransformConcat(existingTransform, CGAffineTransformConcat(t2, t3));
            [layerInstruction setTransform:newTransform atTime:kCMTimeZero];
        }
        
    }
    
    
    // Step 4
    // Add the transform instructions to the video composition
    instruction.layerInstructions = @[layerInstruction];
    self.mutableVideoComposition.instructions = @[instruction];
    
    
    // Step 5
    // Notify AVSEViewController about rotation operation completion
    // [[NSNotificationCenter defaultCenter] postNotificationName:AVSEEditCommandCompletionNotification object:self];
    
    [self performWithAssetExport];
}

- (IBAction)btnAudioClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.isSelected) {
        
        MPVolumeView* volumeView = [[MPVolumeView alloc] init];
        
        //find the volumeSlider
        UISlider* volumeViewSlider = nil;
        for (UIView *view in [volumeView subviews]){
            if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
                volumeViewSlider = (UISlider*)view;
                break;
            }
        }
        
        [volumeViewSlider setValue:0.0f animated:YES];
        [volumeViewSlider sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    else{
        MPVolumeView* volumeView = [[MPVolumeView alloc] init];
        
        //find the volumeSlider
        UISlider* volumeViewSlider = nil;
        for (UIView *view in [volumeView subviews]){
            if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
                volumeViewSlider = (UISlider*)view;
                break;
            }
        }
        
        [volumeViewSlider setValue:1.0f animated:YES];
        [volumeViewSlider sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
}

- (IBAction)btnVideoClicked:(UIButton *)sender {
    _btnVideo.selected = !_btnVideo.selected;
    
    if (sender.selected) {
        //_constraintViewCameraHeight.constant = 0;
        [_viewCamera setHidden:YES];
    }
    else{
        //_constraintViewCameraHeight.constant = _viewVideo.frame.size.height;
        [_viewCamera setHidden:NO];
    }
}

- (void)performWithAssetExport
{
    // Step 1
    // Create an outputURL to which the exported movie will be saved
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *outputURL = paths[0];
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
    outputURL = [outputURL stringByAppendingPathComponent:@"MyRecordedVideo.mov"];
    // Remove Existing File
    [manager removeItemAtPath:outputURL error:nil];
    
    
    // Step 2
    // Create an export session with the composition and write the exported movie to the photo library
    self.exportSession = [[AVAssetExportSession alloc] initWithAsset:[self.mutableComposition copy] presetName:AVAssetExportPreset1280x720];
    
    self.exportSession.videoComposition = self.mutableVideoComposition;
    self.exportSession.audioMix = self.mutableAudioMix;
    self.exportSession.outputURL = [NSURL fileURLWithPath:outputURL];
    self.exportSession.outputFileType=AVFileTypeQuickTimeMovie;
    
    [self.exportSession exportAsynchronouslyWithCompletionHandler:^(void){
        
        switch (self.exportSession.status) {
            case AVAssetExportSessionStatusCompleted:
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"Backhome" object:nil];
                //_strThumbForegroundVideo = _exportSession.outputURL.path;
                _strRecordedVideoPath = _exportSession.outputURL.path;
                NSLog(@"Time To Hit The Merge!!!");
                NSLog(@"Recorded Video : %@",_exportSession.outputURL.path);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
                    [self cropVideo];

                });
            }
                break;
            case AVAssetExportSessionStatusFailed:
            {
                NSLog(@"Failed:%@",self.exportSession.error);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
                    [Global showToast:@"Export failed... Please try again..."];
                });
            }
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Canceled:%@",self.exportSession.error);
                
                break;
            default:
                break;
        }
    }];
    
}


-(void)cropVideo{

    [self.moviePlayer stop];
    [self deleteTmpFile];
    
    NSURL *videoFileUrl = [NSURL fileURLWithPath:_strVideoURL];
    
    AVAsset *anAsset = [[AVURLAsset alloc] initWithURL:videoFileUrl options:nil];
    
    self.exportSession = [[AVAssetExportSession alloc]
                          initWithAsset:anAsset presetName:AVAssetExportPresetPassthrough];
    // Implementation continues.
    
    NSURL *furl = [NSURL fileURLWithPath:self.tmpVideoPath];
    
    self.exportSession.outputURL = furl;
    self.exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    
    NSLog(@"Start : %f",self.startTime);
    NSLog(@"Start : %f",self.stopTime);
    
    CMTime start = CMTimeMakeWithSeconds(self.startTime, anAsset.duration.timescale);
    CMTime duration = CMTimeMakeWithSeconds(self.stopTime-self.startTime, anAsset.duration.timescale);
    CMTimeRange range = CMTimeRangeMake(start, duration);
    self.exportSession.timeRange = range;
   
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];

    [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
        
        switch ([self.exportSession status]) {
            case AVAssetExportSessionStatusFailed:
                NSLog(@"Export failed: %@", [[self.exportSession error] localizedDescription]);
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Export canceled");
                break;
            default:
                NSLog(@"NONE");
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    [[TYMActivityIndicator getInstance] hideActivityIndicator:self.view];

                    NSLog(@"Cropped Video : %@",self.tmpVideoPath);
                    _strCroppedVideoPath = self.tmpVideoPath;

                    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
                    
                    if (_btnVideo.isSelected) {
                        [self mergeAudioVideo];
                    }
                    else{
                        [self getAudioFromVideo];
                    }

                });
                
                break;
        }
    }];
}

-(void)getAudioFromVideo {
    
    NSLog(@"SecondVideo to get audio from :-> %@",self.tmpVideoPath);
    
    AVAsset *myasset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:_strRecordedVideoPath] options:nil];
    
    float startTime = 0;
    float endTime = CMTimeGetSeconds([myasset duration]);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *audioPath = [documentsDirectory stringByAppendingPathComponent:@"MySmackastAudio.m4a"];
    
    AVAssetExportSession *exportSession=[AVAssetExportSession exportSessionWithAsset:myasset presetName:AVAssetExportPresetAppleM4A];
    
    exportSession.outputURL=[NSURL fileURLWithPath:audioPath];
    exportSession.outputFileType=AVFileTypeAppleM4A;
    
    CMTime vocalStartMarker = CMTimeMake((int)(floor(startTime * 100)), 100);
    CMTime vocalEndMarker = CMTimeMake((int)(ceil(endTime * 100)), 100);
    
    CMTimeRange exportTimeRange = CMTimeRangeFromTimeToTime(vocalStartMarker, vocalEndMarker);
    exportSession.timeRange= exportTimeRange;
    if ([[NSFileManager defaultManager] fileExistsAtPath:audioPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:audioPath error:nil];
    }
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        if (exportSession.status==AVAssetExportSessionStatusFailed) {
            NSLog(@"failed");
        }
        else {
            NSLog(@"AudioLocation : %@",audioPath);
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [[TYMActivityIndicator getInstance] hideActivityIndicator:self.view];
                _strAudioPath = audioPath;
                [self DetermineTheOrientation];
                
            });
        }
    }];
}


-(void)DetermineTheOrientation{
    
    if ([Global sharedCenter].isVideoLandscape) {
        [self MergeVideosAndAudioLandscape];
    }
    else{
        [self MergeVideosAndAudioPortrait];
    }
    
    /*
    AVURLAsset *asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:_strCroppedVideoPath]];
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if (size.width == txf.tx && size.height == txf.ty){
        NSLog(@"UIInterfaceOrientationLandscapeRight");
    }
        //return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0){
        NSLog(@"UIInterfaceOrientationLandscapeLeft");

    }
        //return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width){
        NSLog(@"UIInterfaceOrientationPortraitUpsideDown");

    }
        //return UIInterfaceOrientationPortraitUpsideDown;
    else{
        NSLog(@"UIInterfaceOrientationPortrait");

    }
        //return UIInterfaceOrientationPortrait;
    
    ************************************************
    
    
    AVAssetTrack *videoTrack = nil;
    AVURLAsset *asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:_strCroppedVideoPath]];
    NSArray *videoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    
    CMFormatDescriptionRef formatDescription = NULL;
    NSArray *formatDescriptions = [videoTrack formatDescriptions];
    if ([formatDescriptions count] > 0)
        formatDescription = (__bridge CMFormatDescriptionRef)[formatDescriptions objectAtIndex:0];
    
    if ([videoTracks count] > 0)
        videoTrack = [videoTracks objectAtIndex:0];
    
    CGSize trackDimensions = {
        .width = 0.0,
        .height = 0.0,
    };
    trackDimensions = [videoTrack naturalSize];
    
    int width = trackDimensions.width;
    int height = trackDimensions.height;

    if (width>height) {
        NSLog(@"Landscape");
        [self MergeVideosAndAudioLandscape];
    }
    else{
        NSLog(@"Portrait");
        [self MergeVideosAndAudioPortrait];
    }
    */
}

-(void)MergeVideosAndAudioLandscape{
    
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    firstAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:_strRecordedVideoPath]];
    secondAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:_strCroppedVideoPath]];
    
    audioAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:_strAudioPath]];
    
    AVAssetTrack *videoTrack = nil;
    AVURLAsset *asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:_strCroppedVideoPath]];
    NSArray *videoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    
    CMFormatDescriptionRef formatDescription = NULL;
    NSArray *formatDescriptions = [videoTrack formatDescriptions];
    if ([formatDescriptions count] > 0)
        formatDescription = (__bridge CMFormatDescriptionRef)[formatDescriptions objectAtIndex:0];
    
    if ([videoTracks count] > 0)
        videoTrack = [videoTracks objectAtIndex:0];
    
    CGSize trackDimensions = {
        .width = 0.0,
        .height = 0.0,
    };
    trackDimensions = [videoTrack naturalSize];
    
    int backgroundwidth = trackDimensions.width;
    int backgroundheight = trackDimensions.height;
    NSLog(@"Resolution = %d X %d",backgroundwidth ,backgroundheight);
    
    AVAssetTrack *videoTrack1 = nil;
    AVURLAsset *asset1 = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:_strRecordedVideoPath]];
    NSArray *videoTracks1 = [asset1 tracksWithMediaType:AVMediaTypeVideo];
    
    CMFormatDescriptionRef formatDescription1 = NULL;
    NSArray *formatDescriptions1 = [videoTrack1 formatDescriptions];
    if ([formatDescriptions1 count] > 0)
        formatDescription1 = (__bridge CMFormatDescriptionRef)[formatDescriptions1 objectAtIndex:0];
    
    if ([videoTracks1 count] > 0)
        videoTrack1 = [videoTracks1 objectAtIndex:0];
    
    CGSize trackDimensions1 = {
        .width = 0.0,
        .height = 0.0,
    };
    trackDimensions1 = [videoTrack1 naturalSize];
    
    int originalthumbwidth = trackDimensions1.width;
    int originalthumbheight = trackDimensions1.height;
    NSLog(@"Resolution = %d X %d",originalthumbwidth ,originalthumbheight);
    
    float idealthumbWidth = backgroundwidth*0.4;
    float idealPercent = 100*(idealthumbWidth/originalthumbwidth);
    
    float scalePercent = idealPercent/100;
    
    int newThumbWidth = originalthumbwidth*scalePercent;
    int newThumbHeight = backgroundheight;
    
    NSLog(@"Resolution = %d X %d",newThumbWidth ,newThumbHeight);
   
    //resolution
    
    NSError* error = NULL;
    
    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
    
    //Here we are creating the first AVMutableCompositionTrack.See how we are adding a new track to our AVMutableComposition.
    AVMutableCompositionTrack *firstTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    //Now we set the length of the firstTrack equal to the length of the firstAsset and add the firstAsset to out newly created track at kCMTimeZero so video plays from the start of the track.
    
    [firstTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration) ofTrack:[[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:&error];
    
    //Now we repeat the same process for the 2nd track as we did above for the first track.Note that the new track also starts at kCMTimeZero meaning both tracks will play simultaneously.
    AVMutableCompositionTrack *secondTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    [secondTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, secondAsset.duration) ofTrack:[[secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:&error];
    //[secondTrack setPreferredTransform:CGAffineTransformMakeScale(0.25f,0.25f)];
    
    // Grab the source track from AVURLAsset for example.
    AVAssetTrack *assetVideoTrack = [secondAsset tracksWithMediaType:AVMediaTypeVideo].lastObject;
    
    // Apply the original transform.
    if (assetVideoTrack && secondTrack) {
        [secondTrack setPreferredTransform:assetVideoTrack.preferredTransform];
    }
    
    //AUDIO TRACK
    if (audioAsset != nil) {
        AVMutableCompositionTrack *AudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        [AudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration) ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    }
    
    //See how we are creating AVMutableVideoCompositionInstruction object.This object will contain the array of our AVMutableVideoCompositionLayerInstruction objects.You set the duration of the layer.You should add the lenght equal to the lingth of the longer asset in terms of duration.
    AVMutableVideoCompositionInstruction * MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    
    MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, firstAsset.duration);
    
    //We will be creating 2 AVMutableVideoCompositionLayerInstruction objects.Each for our 2 AVMutableCompositionTrack.here we are creating AVMutableVideoCompositionLayerInstruction for out first track.see how we make use of Affinetransform to move and scale our First Track.so it is displayed at the bottom of the screen in smaller size.(First track in the one that remains on top).
    AVMutableVideoCompositionLayerInstruction *FirstlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:firstTrack];
    
    CGAffineTransform Scale = CGAffineTransformMakeScale(scalePercent,scalePercent);
    CGAffineTransform Move = CGAffineTransformMakeTranslation(backgroundwidth-newThumbWidth,backgroundheight-newThumbHeight);
    [FirstlayerInstruction setTransform:CGAffineTransformConcat(Scale,Move) atTime:kCMTimeZero];
    
    //Here we are creating AVMutableVideoCompositionLayerInstruction for out second track.see how we make use of Affinetransform to move and scale our second Track.
    AVMutableVideoCompositionLayerInstruction *SecondlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:secondTrack];
    
    //AVMutableVideoCompositionLayerInstruction *SecondlayerInstruction = [self layerInstructionAfterFixingOrientationForAsset:secondAsset forTrack:secondTrack atTime:secondAsset.duration];
    //SecondlayerInstruction = [self layerInstructionAfterFixingOrientationForAsset:secondAsset forTrack:secondTrack atTime:secondAsset.duration];
    
    CGAffineTransform SecondScale = CGAffineTransformMakeScale(0.60f,1.0f);
    CGAffineTransform SecondMove = CGAffineTransformMakeTranslation(0,0);
    
    [SecondlayerInstruction setTransform:CGAffineTransformConcat(SecondScale,SecondMove) atTime:kCMTimeZero];
    
    //Now we add our 2 created AVMutableVideoCompositionLayerInstruction objects to our AVMutableVideoCompositionInstruction in form of an array.
    MainInstruction.layerInstructions = [NSArray arrayWithObjects:FirstlayerInstruction,SecondlayerInstruction,nil];;
    MainInstruction.backgroundColor = [UIColor whiteColor].CGColor;
    //Now we create AVMutableVideoComposition object.We can add mutiple AVMutableVideoCompositionInstruction to this object.We have only one AVMutableVideoCompositionInstruction object in our example.You can use multiple AVMutableVideoCompositionInstruction objects to add multiple layers of effects such as fade and transition but make sure that time ranges of the AVMutableVideoCompositionInstruction objects dont overlap.
    AVMutableVideoComposition *MainCompositionInst = [AVMutableVideoComposition videoComposition];
    
    MainCompositionInst.instructions = [NSArray arrayWithObject:MainInstruction];
    MainCompositionInst.frameDuration = CMTimeMake(1, 30);
    MainCompositionInst.renderSize = CGSizeMake(backgroundwidth, backgroundheight);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"MergedVideo-%d.mov",arc4random() % 1000]];
    
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL=url;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.videoComposition = MainCompositionInst;
    exporter.shouldOptimizeForNetworkUse = YES;
    [exporter exportAsynchronouslyWithCompletionHandler:^
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             switch (exporter.status)
             {
                 case AVAssetExportSessionStatusFailed:
                     NSLog(@"AVAssetExportSessionStatusFailed");
                     NSLog(@"ExportSessionError: %@", [exporter.error localizedDescription]);
                     
                     break;
                 case AVAssetExportSessionStatusCompleted:{
                     NSLog(@"AVAssetExportSessionStatusCompleted");
                     [[TYMActivityIndicator getInstance] hideActivityIndicator:self.view];
                     
                     NSLog(@"Final Video URL :-> %@", exporter.outputURL.absoluteString);
                     
                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                     UploadVideo *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"UploadVideo"];
                     GoToVC.strFinalVideoPath = exporter.outputURL.absoluteString;
                     [self.navigationController pushViewController:GoToVC animated:YES];
                     
                 }
                     
                     break;
                 case AVAssetExportSessionStatusWaiting:
                     NSLog(@"AVAssetExportSessionStatusWaiting");
                     break;
                 default:
                     break;
             }
         });
     }];
    
}

-(void)MergeVideosAndAudioPortrait{
    
    firstAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:_strRecordedVideoPath]];
    secondAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:_strCroppedVideoPath]];
    audioAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:_strAudioPath]];
    
    //find resolution of BG Video
    
    AVAssetTrack *videoTrack = nil;
    AVURLAsset *asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:_strCroppedVideoPath]];
    NSArray *videoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    
    CMFormatDescriptionRef formatDescription = NULL;
    NSArray *formatDescriptions = [videoTrack formatDescriptions];
    if ([formatDescriptions count] > 0)
        formatDescription = (__bridge CMFormatDescriptionRef)[formatDescriptions objectAtIndex:0];
    
    if ([videoTracks count] > 0)
        videoTrack = [videoTracks objectAtIndex:0];
    
    CGSize trackDimensions = {
        .width = 0.0,
        .height = 0.0,
    };
    trackDimensions = [videoTrack naturalSize];
    
    int backgroundwidth = trackDimensions.width;
    int backgroundheight = trackDimensions.height;
    NSLog(@"Resolution = %d X %d",backgroundwidth ,backgroundheight);
    //Resolution = 640 X 360
    
    
    if ([self isVideoPortrait:secondAsset]) {
        NSLog(@"Motherfuckers never loved us!!! Stupi Stupi");
        
        backgroundheight = trackDimensions.width;
        backgroundwidth = trackDimensions.height;
        
        NSLog(@"Resolution post portrait shit = %d X %d",backgroundwidth ,backgroundheight);
        
    }
    
    //find resolution
    
    AVAssetTrack *videoTrack1 = nil;
    AVURLAsset *asset1 = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:_strRecordedVideoPath]];
    NSArray *videoTracks1 = [asset1 tracksWithMediaType:AVMediaTypeVideo];
    
    CMFormatDescriptionRef formatDescription1 = NULL;
    NSArray *formatDescriptions1 = [videoTrack1 formatDescriptions];
    if ([formatDescriptions1 count] > 0)
        formatDescription1 = (__bridge CMFormatDescriptionRef)[formatDescriptions1 objectAtIndex:0];
    
    if ([videoTracks1 count] > 0)
        videoTrack1 = [videoTracks1 objectAtIndex:0];
    
    CGSize trackDimensions1 = {
        .width = 0.0,
        .height = 0.0,
    };
    trackDimensions1 = [videoTrack1 naturalSize];
    
    int originalthumbwidth = trackDimensions1.width;
    int originalthumbheight = trackDimensions1.height;
    NSLog(@"Resolution = %d X %d",originalthumbwidth ,originalthumbheight);
    
    float idealthumbWidth = backgroundwidth*0.30;
    float idealPercent = 100*(idealthumbWidth/originalthumbwidth);
    
    float scalePercent = idealPercent/100;
    
    int newThumbWidth = originalthumbwidth*scalePercent;
    int newThumbHeight = originalthumbheight*scalePercent;
    
    NSLog(@"Resolution = %d X %d",newThumbWidth ,newThumbHeight);
    
    if ([self isVideoPortrait:secondAsset]) {
        
        idealthumbWidth = backgroundwidth*0.30;
        idealPercent = 100*(idealthumbWidth/originalthumbwidth);
        scalePercent = idealPercent/100;
        
        newThumbWidth = originalthumbwidth*scalePercent;
        newThumbHeight = originalthumbheight*scalePercent;
        
        NSLog(@"Resolution = %d X %d",newThumbWidth ,newThumbHeight);
    }
    
    //resolution
    
    NSError* error = NULL;
    
    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
    
    //Here we are creating the first AVMutableCompositionTrack.See how we are adding a new track to our AVMutableComposition.
    AVMutableCompositionTrack *firstTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    //Now we set the length of the firstTrack equal to the length of the firstAsset and add the firstAsset to out newly created track at kCMTimeZero so video plays from the start of the track.
    
    [firstTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration) ofTrack:[[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:&error];
    
    //Now we repeat the same process for the 2nd track as we did above for the first track.Note that the new track also starts at kCMTimeZero meaning both tracks will play simultaneously.
    AVMutableCompositionTrack *secondTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    [secondTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, secondAsset.duration) ofTrack:[[secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:&error];
    //[secondTrack setPreferredTransform:CGAffineTransformMakeScale(0.25f,0.25f)];
    
    // Grab the source track from AVURLAsset for example.
    AVAssetTrack *assetVideoTrack = [secondAsset tracksWithMediaType:AVMediaTypeVideo].lastObject;
    
    // Apply the original transform.
    if (assetVideoTrack && secondTrack) {
        [secondTrack setPreferredTransform:assetVideoTrack.preferredTransform];
    }
    
    //AUDIO TRACK
    if (audioAsset != nil) {
        AVMutableCompositionTrack *AudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        [AudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration) ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    }
    
    //See how we are creating AVMutableVideoCompositionInstruction object.This object will contain the array of our AVMutableVideoCompositionLayerInstruction objects.You set the duration of the layer.You should add the lenght equal to the lingth of the longer asset in terms of duration.
    AVMutableVideoCompositionInstruction * MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    
    MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, firstAsset.duration);
    
    //We will be creating 2 AVMutableVideoCompositionLayerInstruction objects.Each for our 2 AVMutableCompositionTrack.here we are creating AVMutableVideoCompositionLayerInstruction for out first track.see how we make use of Affinetransform to move and scale our First Track.so it is displayed at the bottom of the screen in smaller size.(First track in the one that remains on top).
    AVMutableVideoCompositionLayerInstruction *FirstlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:firstTrack];
    
    CGAffineTransform Scale = CGAffineTransformMakeScale(scalePercent,scalePercent);
    CGAffineTransform Move = CGAffineTransformMakeTranslation(backgroundwidth-newThumbWidth-10,backgroundheight-newThumbHeight-10);
    [FirstlayerInstruction setTransform:CGAffineTransformConcat(Scale,Move) atTime:kCMTimeZero];
    
    //Here we are creating AVMutableVideoCompositionLayerInstruction for out second track.see how we make use of Affinetransform to move and scale our second Track.
    AVMutableVideoCompositionLayerInstruction *SecondlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:secondTrack];
    
    //AVMutableVideoCompositionLayerInstruction *SecondlayerInstruction = [self layerInstructionAfterFixingOrientationForAsset:secondAsset forTrack:secondTrack atTime:secondAsset.duration];
    //SecondlayerInstruction = [self layerInstructionAfterFixingOrientationForAsset:secondAsset forTrack:secondTrack atTime:secondAsset.duration];
    
    CGAffineTransform SecondScale = CGAffineTransformMakeScale(1.0f,1.0f);
    CGAffineTransform SecondMove = CGAffineTransformMakeTranslation(0,0);
    
    [SecondlayerInstruction setTransform:CGAffineTransformConcat(SecondScale,SecondMove) atTime:kCMTimeZero];
    
    if ([self isVideoPortrait:secondAsset]) {
        [SecondlayerInstruction setTransform:secondTrack.preferredTransform atTime:kCMTimeZero];
    }
    
    //Now we add our 2 created AVMutableVideoCompositionLayerInstruction objects to our AVMutableVideoCompositionInstruction in form of an array.
    MainInstruction.layerInstructions = [NSArray arrayWithObjects:FirstlayerInstruction,SecondlayerInstruction,nil];;
    MainInstruction.backgroundColor = [UIColor whiteColor].CGColor;
    //Now we create AVMutableVideoComposition object.We can add mutiple AVMutableVideoCompositionInstruction to this object.We have only one AVMutableVideoCompositionInstruction object in our example.You can use multiple AVMutableVideoCompositionInstruction objects to add multiple layers of effects such as fade and transition but make sure that time ranges of the AVMutableVideoCompositionInstruction objects dont overlap.
    AVMutableVideoComposition *MainCompositionInst = [AVMutableVideoComposition videoComposition];
    
    MainCompositionInst.instructions = [NSArray arrayWithObject:MainInstruction];
    MainCompositionInst.frameDuration = CMTimeMake(1, 30);
    MainCompositionInst.renderSize = CGSizeMake(backgroundwidth, backgroundheight);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeVideo-%d.mov",arc4random() % 1000]];
    
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL=url;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.videoComposition = MainCompositionInst;
    exporter.shouldOptimizeForNetworkUse = YES;
    [exporter exportAsynchronouslyWithCompletionHandler:^
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             switch (exporter.status)
             {
                 case AVAssetExportSessionStatusFailed:
                     NSLog(@"AVAssetExportSessionStatusFailed");
                     NSLog(@"ExportSessionError: %@", [exporter.error localizedDescription]);
                     
                     break;
                 case AVAssetExportSessionStatusCompleted:{
                     
                     [[TYMActivityIndicator getInstance] hideActivityIndicator:self.view];
                     
                     NSLog(@"AVAssetExportSessionStatusCompleted");
                     NSLog(@"Final Video URL :-> %@", exporter.outputURL.absoluteString);
                     
                     //[self exportDidFinish:exporter];
                     
                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                     UploadVideo *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"UploadVideo"];
                     GoToVC.strFinalVideoPath = exporter.outputURL.absoluteString;
                     [self.navigationController pushViewController:GoToVC animated:YES];
                 }
                     break;
                 case AVAssetExportSessionStatusWaiting:
                     NSLog(@"AVAssetExportSessionStatusWaiting");
                     break;
                 default:
                     break;
             }
         });
     }];
    
}


- (void)exportDidFinish:(AVAssetExportSession*)session
{
    NSLog(@"%ld",(long)session);
    if(session.status == AVAssetExportSessionStatusCompleted){
        NSURL *outputURL = session.outputURL;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
            [library writeVideoAtPathToSavedPhotosAlbum:outputURL
                                        completionBlock:^(NSURL *assetURL, NSError *error){
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                if (error) {
                                                    
                                                    NSLog(@"Final Video URL :-> %@", outputURL);
                                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
                                                    [alert show];
                                                }
                                                else{
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        //[SVProgressHUD dismiss];
                                                        [[TYMActivityIndicator getInstance] hideActivityIndicator:self.view];

                                                        NSLog(@"Final Video URL :-> %@", outputURL);
                                                        
                                                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                                                        UploadVideo *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"UploadVideo"];
                                                        GoToVC.strFinalVideoPath = outputURL.absoluteString;
                                                        [self.navigationController pushViewController:GoToVC animated:YES];
                                                        
                                                    });
                                                }
                                            });
                                            
                                        }];
        }
    }
    
    audioAsset = nil;
    firstAsset = nil;
    secondAsset = nil;
}

-(void)mergeAudioVideo{
    
    AVURLAsset* audioAsset = [[AVURLAsset alloc]initWithURL:outputFileURL options:nil];
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:_strCroppedVideoPath] options:nil];
    
    NSLog(@"%ld", (long)[self orientationForTrack:videoAsset]);
    
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *compositionCommentaryTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
    [compositionCommentaryTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration)
                                        ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
                                         atTime:kCMTimeZero error:nil];
    
    
    
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                                   preferredTrackID:kCMPersistentTrackID_Invalid];
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                   ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
                                    atTime:kCMTimeZero error:nil];
    
    // Grab the source track from AVURLAsset for example.
    AVAssetTrack *assetVideoTrack = [videoAsset tracksWithMediaType:AVMediaTypeVideo].lastObject;
    
    // Apply the original transform.
    if (assetVideoTrack && compositionVideoTrack) {
        [compositionVideoTrack setPreferredTransform:assetVideoTrack.preferredTransform];
    }
    
    AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition
                                                                          presetName:AVAssetExportPresetHighestQuality];
    
    NSString* videoName = @"export.mov";
    
    NSString *exportPath = [NSTemporaryDirectory() stringByAppendingPathComponent:videoName];
    NSURL    *exportUrl = [NSURL fileURLWithPath:exportPath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
    }
    
    _assetExport.outputFileType = @"com.apple.quicktime-movie";
    _assetExport.outputURL = exportUrl;
    _assetExport.shouldOptimizeForNetworkUse = YES;
    
    [_assetExport exportAsynchronouslyWithCompletionHandler:
     ^(void ) {
         // your completion code here
         dispatch_async(dispatch_get_main_queue(), ^{
             //[self exportDidFinish:_assetExport];
             
             [[TYMActivityIndicator getInstance] hideActivityIndicator:self.view];
             
             NSLog(@"Final Video URL :-> %@", _assetExport.outputURL.absoluteString);
             
             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
             UploadVideo *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"UploadVideo"];
             GoToVC.strFinalVideoPath = _assetExport.outputURL.absoluteString;
             [self.navigationController pushViewController:GoToVC animated:YES];             
             
         });
     }
     ];
}
- (UIInterfaceOrientation)orientationForTrack:(AVAsset *)asset
{
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if (size.width == txf.tx && size.height == txf.ty)
        return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIInterfaceOrientationPortrait;
}

-(BOOL) isVideoPortrait:(AVAsset *)asset
{
    BOOL isPortrait = FALSE;
    NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    if([tracks    count] > 0) {
        AVAssetTrack *videoTrack = [tracks objectAtIndex:0];
        
        CGAffineTransform t = videoTrack.preferredTransform;
        // Portrait
        if(t.a == 0 && t.b == 1.0 && t.c == -1.0 && t.d == 0)
        {
            isPortrait = YES;
        }
        // PortraitUpsideDown
        if(t.a == 0 && t.b == -1.0 && t.c == 1.0 && t.d == 0)  {
            
            isPortrait = YES;
        }
        // LandscapeRight
        if(t.a == 1.0 && t.b == 0 && t.c == 0 && t.d == 1.0)
        {
            isPortrait = NO;
        }
        // LandscapeLeft
        if(t.a == -1.0 && t.b == 0 && t.c == 0 && t.d == -1.0)
        {
            isPortrait = NO;
        }
    }
    return isPortrait;
}

#pragma mark - Other
-(void)deleteTmpFile{
    
    NSURL *url = [NSURL fileURLWithPath:self.tmpVideoPath];
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL exist = [fm fileExistsAtPath:url.path];
    NSError *err;
    if (exist) {
        [fm removeItemAtURL:url error:&err];
        NSLog(@"file deleted");
        if (err) {
            NSLog(@"file remove error, %@", err.localizedDescription );
        }
    } else {
        NSLog(@"no file by that name");
    }
}


- (IBAction)btnBackClicked:(id)sender {
    [self pauseVideo];
    [self.moviePlayer stop];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
