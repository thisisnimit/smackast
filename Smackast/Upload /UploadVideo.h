//
//  UploadVideo.h
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseVideo.h"
#import "JVFloatLabeledTextView.h"
#import "ShareController.h"

@interface UploadVideo : UIViewController{
    NSString *strUserName;
}
@property (weak, nonatomic) IBOutlet UIView *viewVideo;
- (IBAction)btnCloseClicked:(id)sender;
- (IBAction)btnUploadOnYoutubeClicked:(id)sender;

@property (strong, nonatomic) NSString *strFinalVideoPath;

@property (weak, nonatomic) IBOutlet UIButton *btnPlay;

@property (weak, nonatomic) IBOutlet UILabel *lblWordCount;
@property (strong, nonatomic) UILabel *lblPlaceHolder;
- (IBAction)btnShareClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *txtVideoName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *txtLink;

@end
