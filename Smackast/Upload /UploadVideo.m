//
//  UploadVideo.m
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "UploadVideo.h"
@import MediaPlayer;

@interface UploadVideo ()
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;

@end

@implementation UploadVideo

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"Video URL : %@",_strFinalVideoPath);
    
    
    [_btnShare setHidden:YES];
    [_txtLink setHidden:YES];

    [self setUpPlaceholders];
    
    NSURL *url = [NSURL URLWithString:_strFinalVideoPath];
    [self moviePlayer:url];
    [self playVideo];

}

-(void)setUpPlaceholders{
    
    _txtLink.floatingLabelActiveTextColor = KAppThemeColor;
    _txtLink.font = [UIFont fontWithName:KRobotoRegular size:18.0];
    
    _txtVideoName.floatingLabelActiveTextColor = KAppThemeColor;
    _txtVideoName.font = [UIFont fontWithName:KRobotoRegular size:18.0];
    _txtVideoName.placeholderLabel.text = @"Video Title";

}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.moviePlayer stop];
}

- (void) mymoviePlayBackDidFinish:(NSNotification*)notification {
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:player];
    
    NSLog(@"Video ended!!!");
    
    [_btnPlay setSelected:NO];
    //[_btnPlay setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [_btnPlay setImage:nil forState:UIControlStateNormal];
    [self playVideo];

}


#pragma mark -
#pragma mark PlayVideo

- (void)moviePlayer:(NSURL*)url
{
    NSLog(@"Final URL To Play Video :-> %@",[url absoluteString]);
    
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    self.moviePlayer.movieSourceType = MPMediaTypeAnyVideo;
    self.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    self.moviePlayer.repeatMode = MPMovieRepeatModeNone;
    self.moviePlayer.allowsAirPlay = NO;
    self.moviePlayer.shouldAutoplay = NO;
    self.moviePlayer.controlStyle = MPMovieControlStyleNone;
    self.moviePlayer.view.frame = self.viewVideo.bounds;
    self.moviePlayer.view.autoresizingMask = (UIViewAutoresizing)(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.viewVideo addSubview:self.moviePlayer.view];
    
    MPVolumeView* volumeView = [[MPVolumeView alloc] init];
    
    //find the volumeSlider
    UISlider* volumeViewSlider = nil;
    for (UIView *view in [volumeView subviews]){
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
            volumeViewSlider = (UISlider*)view;
            break;
        }
    }
    
    [volumeViewSlider setValue:1.0f animated:YES];
    [volumeViewSlider sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    [self.moviePlayer prepareToPlay];
    [self addPlayPauseButton];
}

-(void)addPlayPauseButton{
    [_btnPlay addTarget:self
                 action:@selector(btnMyPlayPauseClicked:)
       forControlEvents:UIControlEventTouchUpInside];
    //[_btnPlay setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [_btnPlay setImage:nil forState:UIControlStateNormal];

    [_viewVideo addSubview:_btnPlay];
}

-(void) btnMyPlayPauseClicked:(UIButton*)sender
{
    NSLog(@"you clicked on button %ld", (long)sender.tag);
    sender.selected = !sender.selected;
    
    if (sender.isSelected) {
        [self playVideo];
    }
    else{
        [self pauseVideo];
    }
}

-(void)playVideo{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mymoviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_moviePlayer];
    [_btnPlay setSelected:YES];
    [_btnPlay setImage:nil forState:UIControlStateNormal];
    [self.moviePlayer play];
}

-(void)pauseVideo{
    [_btnPlay setSelected:NO];
    //[_btnPlay setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [_btnPlay setImage:nil forState:UIControlStateNormal];

    [self.moviePlayer pause];
}

- (IBAction)btnCloseClicked:(id)sender {
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[ChooseVideo class]]) {
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}

- (IBAction)btnUploadOnYoutubeClicked:(id)sender {
    
    [self uploadVideo];
}

-(NSString*)getDuration:(NSString*)strURL{
    NSURL *sourceMovieURL = [NSURL fileURLWithPath:strURL];
    AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:sourceMovieURL options:nil];
    
    CMTime duration = sourceAsset.duration;
    float seconds = CMTimeGetSeconds(duration);
    
    int totalSeconds = (int)ceilf(seconds);
    NSString *strTotalSeconds = [self timeFormatted:totalSeconds];
    
    return strTotalSeconds;
}

- (NSString *)timeFormatted:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours == 0) {
        return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    }
    else{
        return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    }
}

-(NSString *)getRandomPINString
{
    NSMutableString *returnString = [NSMutableString stringWithCapacity:6];
    
    NSString *numbers = @"0123456789";
    
    // First number cannot be 0
    [returnString appendFormat:@"%C", [numbers characterAtIndex:(arc4random() % ([numbers length]-1))+1]];
    
    for (int i = 1; i < 6; i++)
    {
        [returnString appendFormat:@"%C", [numbers characterAtIndex:arc4random() % [numbers length]]];
    }
    NSLog(@"VideoThumb name :-> %@.mov/png",returnString);
    return returnString;
}


-(void)uploadVideo{
    
    if ([_txtVideoName.text isEqualToString:@""]) {
        [Global showToast:@"Please enter your video name"];
        return;
    }
    
    [self pauseVideo];
    [_btnUpload setTitle:@"UPLOADING..." forState:UIControlStateNormal];
    
    CABasicAnimation *theAnimation=[CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    theAnimation.duration=1.0;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue= [UIColor colorWithHexString:@"0014FF"];
    theAnimation.toValue= [UIColor colorWithHexString:@"001dff"];
    [_btnUpload.layer addAnimation:theAnimation forKey:@"ColorPulse"];
    
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];

    NSLog(@"%@",_strFinalVideoPath);
    NSURL *url = [NSURL URLWithString:_strFinalVideoPath];

    NSString *strDuration = [self getDuration:url.path];

    NSString *strURL = [NSString stringWithFormat:@"%@%@?i_user_id=%@&v_title=%@&v_duration=%@",KBaseURL,KUploadVideo,[Global sharedCenter].strUserID,_txtVideoName.text,strDuration];
    strURL = [strURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSData *imageDatacall = [NSData dataWithContentsOfFile:url.path];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:strURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        // set Content-Type in HTTP header
        //Set Params
        [formData appendPartWithFileData:imageDatacall name:@"v_video" fileName:[NSString stringWithFormat:@"%@.mov",[self getRandomPINString]] mimeType:@"video/quicktime"];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    AFHTTPResponseSerializer *serializer = [[AFHTTPResponseSerializer alloc] init];
    serializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", @"text/json", @"text/javascript",nil];
    manager.responseSerializer = serializer;
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //[progressView setProgress:uploadProgress.fractionCompleted];
                          [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
                          [SVProgressHUD showProgress:uploadProgress.fractionCompleted status:[NSString stringWithFormat:@"%.1f%%\nUploading...",uploadProgress.fractionCompleted*100] maskType:SVProgressHUDMaskTypeBlack];
                          NSLog(@"%f",uploadProgress.fractionCompleted); 
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      [SVProgressHUD dismiss];
                      
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
                          NSLog(@"%@ %@", response, responseObject);
                          
                          if (response == nil) {
                              //imageData have some value
                              [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
                              [Global showToast:@"Oops! Upload fail... Please try again."];
                              return;
                          }
                          
                          id result = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                      options:NSJSONReadingAllowFragments
                                                                        error:NULL];
                          NSLog(@"result = %@", result);
                          
                          if([[result valueForKey:@"success"] integerValue] == 1)
                          {
                              //[Global showToast:[result valueForKey:@"msg"]];
                              NSLog(@"Video ID : %@",[result valueForKey:@"id"]);
                              
                              [_btnUpload setBackgroundColor:[UIColor colorWithHexString:@"49b961"]];
                              [_btnUpload setTitle:@"UPLOADED!" forState:UIControlStateNormal];

                              CABasicAnimation *theAnimation=[CABasicAnimation animationWithKeyPath:@"backgroundColor"];
                              theAnimation.duration=1.0;
                              theAnimation.repeatCount=HUGE_VALF;
                              theAnimation.autoreverses=YES;
                              theAnimation.fromValue= [UIColor colorWithHexString:@"49b961"];
                              theAnimation.toValue= [UIColor colorWithHexString:@"49b979"];
                              [_btnUpload.layer addAnimation:theAnimation forKey:@"ColorPulse"];
                              
                              double delayInSeconds = 1.0;
                              dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                              dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

                                  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                                  ShareController *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"ShareController"];
                                  GoToVC.strLink = [NSString stringWithFormat:@"%s%@","https://www.youtube.com/watch?v=",[result valueForKey:@"youtube_id"]];
                                  GoToVC.strVideoName = _txtVideoName.text;
                                  [self.navigationController pushViewController:GoToVC animated:YES];
                              });
                              
                              
                        }
                          else
                          {
                              [Global showToast:[result valueForKey:@"msg"]];
                          }
                          
                      }
                  }];
    
    [uploadTask resume];
    
}


- (IBAction)btnShareClicked:(id)sender {
    
    NSString *textToShare = @"Hey! Check out this awesome video from Smackast";
    NSURL *myWebsite = [NSURL URLWithString:_txtLink.text];

    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    [self presentViewController:activityVC animated:YES completion:nil];
}
@end
