//
//  ProfileController.m
//  Smackast
//
//  Created by Nimit on 23/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "ProfileController.h"

@interface ProfileController ()

@end

@implementation ProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_txtEmail setUserInteractionEnabled:NO];
    [_txtUserName setUserInteractionEnabled:NO];
    _imageUpdated = NO;
    
    [self WSGetProfile];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    _imgProfile.layer.cornerRadius = 65.0;
    _imgProfile.clipsToBounds = YES;

}

-(void)WSGetProfile{
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@?user_id=%@",KBaseURL,KGetProfile,[Global sharedCenter].strUserID];
    
    NSLog(@"%@",strURL);
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [Global callAPIwithURL_PostRequest:strURL completion:^(id responseObject, BOOL sucess)
     {
         [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
         NSLog(@"%@ response :::\n%@",KGetProfile,responseObject);
         if(sucess)
         {
             if([[responseObject valueForKey:@"success"] integerValue] == 1)
             {
                 _txtUserName.text = [responseObject valueForKey:@"v_username"];
                 _txtEmail.text = [responseObject valueForKey:@"v_email"];
                 //_txtName.text = [responseObject valueForKey:@"v_full_name"];
                 
                 NSString *str = [responseObject valueForKey:@"v_image"];
                 NSString *stringWithoutSpaces = [str stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                 NSURL *url = [NSURL URLWithString:stringWithoutSpaces];
                 NSURLRequest *request = [NSURLRequest requestWithURL:url];
                 UIImage *placeholderImage = [UIImage imageNamed:@"profilePlaceholder"];
                 
                 [_imgProfile setImageWithURLRequest:request
                                    placeholderImage:placeholderImage
                                             success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                 _imgProfile.image = image;
                                             } failure:nil];
             }
             else
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
             }
         }
         else
         {
             [Global showToast:KMSG_server_is_not_responding];
         }
     }];
}


- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnProfileClicked:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self openCamera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self openGallery];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];

}

-(void)openCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker
                           animated:YES completion:nil];
        _newMedia = YES;
    }
}

-(void)openGallery
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker
                           animated:YES completion:nil];
        _newMedia = NO;
    }
}

#pragma mark - UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        imgChosenImage = info[UIImagePickerControllerOriginalImage];
        
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        
        controller.keepingCropAspectRatio = YES;
        
        CGFloat width = imgChosenImage.size.width;
        CGFloat height = imgChosenImage.size.height;
        CGFloat length = MIN(width, height);
        controller.imageCropRect = CGRectMake((width - length) / 2,
                                              (height - length) / 2,
                                              length,
                                              length);
        controller.image=imgChosenImage;
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        [self presentViewController:navigationController animated:YES completion:NULL];
        
        _imgProfile.image = imgChosenImage;
        _imageUpdated = YES;
        if (_newMedia)
            UIImageWriteToSavedPhotosAlbum(imgChosenImage,
                                           self,
                                           @selector(image:finishedSavingWithError:contextInfo:),
                                           nil);
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        // Code here to support video if enabled
    }
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    _imgProfile.image = croppedImage;
    
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        NSLog(@"Error in saving image file : %@", error);
    }
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)WSSetProfile{
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@?i_user_id=%@&v_full_name=%@&v_email=%@&v_username=%@",KBaseURL,KSetProfile,[Global sharedCenter].strUserID, @"NAME",_txtEmail.text,_txtUserName.text];
    
    NSLog(@"%@",strURL);
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [Global callAPIwithURL_PostRequest:strURL completion:^(id responseObject, BOOL sucess)
     {
         [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
         NSLog(@"%@ response :::\n%@",KSetProfile,responseObject);
         if(sucess)
         {
             if([[responseObject valueForKey:@"success"] integerValue] == 1)
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
                 [self.navigationController popViewControllerAnimated:YES];
             }
             else
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
             }
         }
         else
         {
             [Global showToast:KMSG_server_is_not_responding];
         }
     }];
}

-(void)WSSetProfileWithImage
{
    NSString *baseurl = [NSString stringWithFormat:@"%@%@?i_user_id=%@&v_full_name=%@&v_email=%@&v_username=%@",KBaseURL,KSetProfile,[Global sharedCenter].strUserID, @"NAME",_txtEmail.text,_txtUserName.text];
    baseurl = [baseurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *FileParamConstant = @"v_image";
    NSData *imageDatacall = [[NSData alloc] initWithData:UIImageJPEGRepresentation((_imgProfile.image), 1.0)];
    
    long imageSize = imageDatacall.length;
    NSLog(@"SIZE OF IMAGE: %li ", imageSize);
    
    //1 it represents the quality of the image.
    if (imageSize >4000000)
    {
        imageDatacall = UIImageJPEGRepresentation(_imgProfile.image, 0.3);
        long imageSize1 = imageDatacall.length;
        NSLog(@"SIZE OF 1 IMAGE: %li ", imageSize1);
    }
    else if (imageSize >1000000)
    {
        imageDatacall = UIImageJPEGRepresentation(_imgProfile.image, 0.5);
        long imageSize1 = imageDatacall.length;
        NSLog(@"SIZE OF 1 IMAGE: %li ", imageSize1);
    }
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)imageDatacall.length );
    //Assuming data is not nil we add this to the multipart form
    
    // post body
    NSMutableData *body = [NSMutableData data];
    if (imageDatacall)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.png\"\r\n", FileParamConstant,[self getRandomPINString]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageDatacall];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:baseurl]];
    if ([[Global sharedCenter] checkInternetConnectivity])
    {
        [[TYMActivityIndicator getInstance]showActivityIndicator:self.view];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (data==nil) {
                 [Global showToast:@"Oops! Something went wrong... Please try again."];
                 [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
                 
                 return;
             }
             
             id result = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingAllowFragments
                                                           error:NULL];
             NSLog(@"[result class] = %@", [result class]);
             NSLog(@"result = %@", result);
             
             [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
             
             if([[result valueForKey:@"success"] integerValue] == 1)
             {
                 NSLog(@"Success");
                 [Global showToast:[result valueForKey:@"msg"]];
                 [self.navigationController popViewControllerAnimated:YES];
             }
             else
             {
                 [Global showToast:[result valueForKey:@"msg"]];
             }
         }];
    }
}

-(NSString *)getRandomPINString
{
    NSMutableString *returnString = [NSMutableString stringWithCapacity:6];
    
    NSString *numbers = @"0123456789";
    
    // First number cannot be 0
    [returnString appendFormat:@"%C", [numbers characterAtIndex:(arc4random() % ([numbers length]-1))+1]];
    
    for (int i = 1; i < 6; i++)
    {
        [returnString appendFormat:@"%C", [numbers characterAtIndex:arc4random() % [numbers length]]];
    }
    NSLog(@"Image name :-> %@.png",returnString);
    return returnString;
}

- (IBAction)btnSaveClicked:(id)sender {
    if (_imageUpdated) {
        [self WSSetProfileWithImage];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)btnLogOutClicked:(id)sender {

    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[SignIn class]]) {
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}
@end
