//
//  ProfileController.h
//  Smackast
//
//  Created by Nimit on 23/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "PECropViewController.h"
#import "SignIn.h"


@interface ProfileController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, PECropViewControllerDelegate>{
    UIImage *imgChosenImage;
}

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnProfileClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@property BOOL newMedia;
@property BOOL imageUpdated;

- (IBAction)btnLogOutClicked:(id)sender;


@end
