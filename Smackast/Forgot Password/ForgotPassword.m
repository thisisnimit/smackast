//
//  ForgotPassword.m
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "ForgotPassword.h"

@interface ForgotPassword ()

@end

@implementation ForgotPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpPlaceholders];
    
}

-(void)setUpPlaceholders{
    _txtEmail.floatingLabelActiveTextColor = KAppThemeColor;
    _txtEmail.font = [UIFont fontWithName:KRobotoRegular size:18.0];
}

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSubmitClicked:(id)sender {
    
    if ([self isValidated]) {
        [self WSForgotPassword];
    }
}

-(void)WSForgotPassword{
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@?v_email=%@",KBaseURL,KForgotPassword,_txtEmail.text];
    NSLog(@"%@",strURL);
    [[TYMActivityIndicator getInstance] showActivityIndicator:self.view];
    
    [Global callAPIwithURL_PostRequest:strURL completion:^(id responseObject, BOOL sucess)
     {
         [[TYMActivityIndicator getInstance]hideActivityIndicator:self.view];
         NSLog(@"%@ response :::\n%@",KForgotPassword,responseObject);
         if(sucess)
         {
             if([[responseObject valueForKey:@"success"] integerValue] == 1)
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
                 
                 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                 ResetPassword *GoToVC=[mainStoryboard instantiateViewControllerWithIdentifier:@"ResetPassword"];
                 GoToVC.strEmail = _txtEmail.text;
                 [self.navigationController pushViewController:GoToVC animated:YES];
             }
             else
             {
                 [Global showToast:[responseObject valueForKey:@"msg"]];
             }
         }
         else
         {
             [Global showToast:KMSG_server_is_not_responding];
         }
     }];
    
}
-(BOOL)isValidated{
    
    BOOL result = [[Global sharedCenter] validateEmailWithString:_txtEmail.text];
    
    if (_txtEmail.text.length == 0) {
        [Global showToast:@"Please enter email address"];
        return NO;
    }
    else if(!result){
        [Global showToast:@"Please enter valid email"];
        return NO;
    }
    
    return YES;
}

@end
