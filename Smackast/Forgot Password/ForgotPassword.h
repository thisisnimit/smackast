//
//  ForgotPassword.h
//  Smackast
//
//  Created by Nimit on 16/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResetPassword.h"
#import "JVFloatLabeledTextField.h"

@interface ForgotPassword : UIViewController
- (IBAction)btnSubmitClicked:(id)sender;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *txtEmail;

@end
