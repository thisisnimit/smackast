//
//  ShareViewController.m
//  Add to Smackast
//
//  Created by Nimit on 17/03/17.
//  Copyright © 2017 Nimit. All rights reserved.
//

#import "ShareViewController.h"
@import MobileCoreServices;
static NSString *const AppGroupId = @"group.smackast.share";

@interface ShareViewController ()

@end

@implementation ShareViewController

- (BOOL)isContentValid
{
    // Do validation of contentText and/or NSExtensionContext attachments here
    return YES;
}

-(void)viewDidLoad
{
    sharedUserDefaults = [[NSUserDefaults alloc] initWithSuiteName:AppGroupId];
}

- (void)didSelectPost
{
    
    inputItem = self.extensionContext.inputItems.firstObject;
    
    NSItemProvider *urlItemProvider = [[inputItem.userInfo valueForKey:NSExtensionItemAttachmentsKey] objectAtIndex:0];
    
    
    if([urlItemProvider hasItemConformingToTypeIdentifier:@"public.plain-text"]) {
        NSLog(@"itemprovider = %@", urlItemProvider);
        
        [urlItemProvider loadItemForTypeIdentifier:@"public.plain-text" options:nil completionHandler: ^(id<NSSecureCoding> item, NSError *error) {
            
            NSString *url;
            if([(NSObject*)item isKindOfClass:[NSString class]]) {
                url = (NSString*)item;
                NSLog(@"URL : %@",url);
                
                NSLog(@"Validated : %@",url);
                [sharedUserDefaults removeObjectForKey:@"SharedExtension"];
                
                NSMutableArray *arrSites;
                if ([sharedUserDefaults valueForKey:@"SharedExtension"])
                    arrSites = [sharedUserDefaults valueForKey:@"SharedExtension"];
                else
                    arrSites = [[NSMutableArray alloc] init];
                
                [arrSites addObject:url];
                [sharedUserDefaults setObject:arrSites forKey:@"SharedExtension"];
                [sharedUserDefaults synchronize];
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Success"
                                              message:@"Posted Successfully."
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [UIView animateWithDuration:0.20 animations:^
                                          {
                                              self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
                                          }
                                                          completion:^(BOOL finished)
                                          {
                                              [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
                                          }];
                                     }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }];
    }
    else if([urlItemProvider hasItemConformingToTypeIdentifier:@"public.url"]) {
        
        NSLog(@"itemprovider = %@", urlItemProvider);
        
        [urlItemProvider loadItemForTypeIdentifier:@"public.url" options:nil completionHandler: ^(id<NSSecureCoding> item, NSError *error) {
            
            NSURL *url;
            if([(NSObject*)item isKindOfClass:[NSURL class]]) {
                url = (NSURL*)item;
                NSLog(@"URL : %@",url);
                NSLog(@"URL : %@",url.absoluteString);
                
                NSLog(@"Validated : %@",url);
                [sharedUserDefaults removeObjectForKey:@"SharedExtension"];
                
                NSMutableArray *arrSites;
                if ([sharedUserDefaults valueForKey:@"SharedExtension"])
                    arrSites = [sharedUserDefaults valueForKey:@"SharedExtension"];
                else
                    arrSites = [[NSMutableArray alloc] init];

                [arrSites addObject:url.absoluteString];
                
                [sharedUserDefaults setObject:arrSites forKey:@"SharedExtension"];
                [sharedUserDefaults synchronize];
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Success"
                                              message:@"Posted Successfully."
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [UIView animateWithDuration:0.20 animations:^
                                          {
                                              self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
                                          }
                                                          completion:^(BOOL finished)
                                          {
                                              [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
                                          }];
                                     }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
    }
    else{
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Ooops!"
                                      message:@"Please share a valid URL."
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [UIView animateWithDuration:0.20 animations:^
                                  {
                                      self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
                                  }
                                                  completion:^(BOOL finished)
                                  {
                                      [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
                                  }];
                             }];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (NSString*)extractYoutubeIdFromLink:(NSString* )link
{
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,link.length)];
    
    if (array.count > 0)
    {
        NSTextCheckingResult *result = array.firstObject;
        return [link substringWithRange:result.range];
    }
    return nil;
}


- (NSArray *)configurationItems {
    // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
    return @[];
}

@end
